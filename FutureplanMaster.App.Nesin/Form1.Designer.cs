﻿namespace FutureplanMaster.App.Nesin
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOffUnitResult = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFinalCalc = new System.Windows.Forms.Button();
            this.txtFinalCalcIdx = new System.Windows.Forms.TextBox();
            this.txtMasterIdx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnMasterCalc = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBatchScore = new System.Windows.Forms.Button();
            this.txtGrdSc = new System.Windows.Forms.TextBox();
            this.txtIdx = new System.Windows.Forms.TextBox();
            this.btnBatchTest = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnMakeBatchScore = new System.Windows.Forms.Button();
            this.btnNonCalc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOffUnitResult
            // 
            this.txtOffUnitResult.Location = new System.Drawing.Point(37, 79);
            this.txtOffUnitResult.Multiline = true;
            this.txtOffUnitResult.Name = "txtOffUnitResult";
            this.txtOffUnitResult.Size = new System.Drawing.Size(1129, 588);
            this.txtOffUnitResult.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(248, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "finalIDx";
            // 
            // btnFinalCalc
            // 
            this.btnFinalCalc.Location = new System.Drawing.Point(454, 22);
            this.btnFinalCalc.Name = "btnFinalCalc";
            this.btnFinalCalc.Size = new System.Drawing.Size(69, 49);
            this.btnFinalCalc.TabIndex = 8;
            this.btnFinalCalc.Text = "산출";
            this.btnFinalCalc.UseVisualStyleBackColor = true;
            this.btnFinalCalc.Click += new System.EventHandler(this.btnFinalCalc_Click);
            // 
            // txtFinalCalcIdx
            // 
            this.txtFinalCalcIdx.Location = new System.Drawing.Point(297, 37);
            this.txtFinalCalcIdx.Name = "txtFinalCalcIdx";
            this.txtFinalCalcIdx.Size = new System.Drawing.Size(153, 21);
            this.txtFinalCalcIdx.TabIndex = 7;
            // 
            // txtMasterIdx
            // 
            this.txtMasterIdx.Location = new System.Drawing.Point(697, 35);
            this.txtMasterIdx.Name = "txtMasterIdx";
            this.txtMasterIdx.Size = new System.Drawing.Size(153, 21);
            this.txtMasterIdx.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(633, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "masterIDx";
            // 
            // btnMasterCalc
            // 
            this.btnMasterCalc.Location = new System.Drawing.Point(855, 22);
            this.btnMasterCalc.Name = "btnMasterCalc";
            this.btnMasterCalc.Size = new System.Drawing.Size(69, 49);
            this.btnMasterCalc.TabIndex = 12;
            this.btnMasterCalc.Text = "산출";
            this.btnMasterCalc.UseVisualStyleBackColor = true;
            this.btnMasterCalc.Click += new System.EventHandler(this.btnMasterCalc_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "2021",
            "2020",
            "2019"});
            this.comboBox1.Location = new System.Drawing.Point(96, 37);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(139, 20);
            this.comboBox1.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "년도";
            // 
            // btnBatchScore
            // 
            this.btnBatchScore.Location = new System.Drawing.Point(930, 22);
            this.btnBatchScore.Name = "btnBatchScore";
            this.btnBatchScore.Size = new System.Drawing.Size(69, 49);
            this.btnBatchScore.TabIndex = 15;
            this.btnBatchScore.Text = "배치점수";
            this.btnBatchScore.UseVisualStyleBackColor = true;
            this.btnBatchScore.Click += new System.EventHandler(this.btnBatchScore_Click);
            // 
            // txtGrdSc
            // 
            this.txtGrdSc.Location = new System.Drawing.Point(1036, 48);
            this.txtGrdSc.Name = "txtGrdSc";
            this.txtGrdSc.Size = new System.Drawing.Size(100, 21);
            this.txtGrdSc.TabIndex = 16;
            // 
            // txtIdx
            // 
            this.txtIdx.Location = new System.Drawing.Point(1142, 48);
            this.txtIdx.Name = "txtIdx";
            this.txtIdx.Size = new System.Drawing.Size(100, 21);
            this.txtIdx.TabIndex = 17;
            // 
            // btnBatchTest
            // 
            this.btnBatchTest.Location = new System.Drawing.Point(1267, 22);
            this.btnBatchTest.Name = "btnBatchTest";
            this.btnBatchTest.Size = new System.Drawing.Size(69, 49);
            this.btnBatchTest.TabIndex = 18;
            this.btnBatchTest.Text = "산출";
            this.btnBatchTest.UseVisualStyleBackColor = true;
            this.btnBatchTest.Click += new System.EventHandler(this.btnBatchTest_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1037, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "등급";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1142, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 12);
            this.label3.TabIndex = 20;
            this.label3.Text = "IDX";
            // 
            // btnMakeBatchScore
            // 
            this.btnMakeBatchScore.Location = new System.Drawing.Point(1159, 114);
            this.btnMakeBatchScore.Name = "btnMakeBatchScore";
            this.btnMakeBatchScore.Size = new System.Drawing.Size(154, 169);
            this.btnMakeBatchScore.TabIndex = 21;
            this.btnMakeBatchScore.Text = "배치점수만들기";
            this.btnMakeBatchScore.UseVisualStyleBackColor = true;
            this.btnMakeBatchScore.Click += new System.EventHandler(this.btnMakeBatchScore_Click);
            // 
            // btnNonCalc
            // 
            this.btnNonCalc.Location = new System.Drawing.Point(529, 22);
            this.btnNonCalc.Name = "btnNonCalc";
            this.btnNonCalc.Size = new System.Drawing.Size(98, 49);
            this.btnNonCalc.TabIndex = 22;
            this.btnNonCalc.Text = "비교과확인";
            this.btnNonCalc.UseVisualStyleBackColor = true;
            this.btnNonCalc.Click += new System.EventHandler(this.btnNonCalc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1468, 807);
            this.Controls.Add(this.btnNonCalc);
            this.Controls.Add(this.btnMakeBatchScore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBatchTest);
            this.Controls.Add(this.txtIdx);
            this.Controls.Add(this.txtGrdSc);
            this.Controls.Add(this.btnBatchScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnMasterCalc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMasterIdx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnFinalCalc);
            this.Controls.Add(this.txtFinalCalcIdx);
            this.Controls.Add(this.txtOffUnitResult);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtOffUnitResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFinalCalc;
        private System.Windows.Forms.TextBox txtFinalCalcIdx;
        private System.Windows.Forms.TextBox txtMasterIdx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnMasterCalc;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBatchScore;
        private System.Windows.Forms.TextBox txtGrdSc;
        private System.Windows.Forms.TextBox txtIdx;
        private System.Windows.Forms.Button btnBatchTest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnMakeBatchScore;
        private System.Windows.Forms.Button btnNonCalc;
    }
}

