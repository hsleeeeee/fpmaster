﻿
using FPFrame.Test;
using MainApp.Common;
using MainApp.Common.Connection;
using MainApp.Common.Util;
using MainApp.Score;
using MainApp.UnivProcs19;
using MainApp.UnivProcs19.AutoCalNs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FutureplanMaster.App.JungSi
{
    public partial class frmIPSIResult : Form
    {
        public DefaultSettingValues config = new DefaultSettingValues
        {
            //CS06_MYJIWON_JUNGSI_IMIT_REAL = "futureplan_moi.dbo.CS06_MYJIWON_JUNGSI_IMIT_REAL_2017_back"
            //CS06_MYJIWON_JUNGSI_IMIT_REAL = "futureplan_moi.dbo.[CS06_MYJIWON_JUNGSI_IMIT_REAL_2016_COMPACT]"
            //CS06_MYJIWON_JUNGSI_IMIT_REAL = "futureplan_moi.dbo.[CS06_MYJIWON_JUNGSI_IMIT_REAL_YEAR_COMPACT]"
        };
        public frmIPSIResult()
        {
            InitializeComponent();
        }

        //3개년입시결과 배치점수
        //타사점수배치점수
        private void btnExe_Click(object sender, EventArgs e)
        {
            string univName = this.txtUnivName.Text;
            univName = string.IsNullOrEmpty(univName) ? string.Empty : univName;

            string linkCd = this.txtLinkCd.Text;
            linkCd = string.IsNullOrEmpty(linkCd) ? string.Empty : linkCd;

            string whereQuery = this.txtWhereQuery.Text;


            if (chk_2016.Checked)
            {
                //makeIpsiResult("2016", univName, linkCd, whereQuery);
            }
            if (chk_2017.Checked)
            {
                //makeIpsiResult("2017", univName, linkCd, whereQuery);
            }
            if (chk_2018.Checked)
            {
                makeIpsiResult("2018", univName, linkCd, whereQuery);
            }
            if (chk_2019.Checked)
            {
                //makeIpsiResult("2019", univName, linkCd, whereQuery);
            }
        }


        private void makeIpsiResult(string year, string univName, string linkCd, string whereQuery)
        {

            //string imitTableName = config.CS06_MYJIWON_JUNGSI_IMIT_REAL.Replace("YEAR", year);
            string imitTableName = string.Format("FPmaster.DBO.VW_IPSI_RESULT");

            //이화여대, 중앙대, 청운대, 한국외대, 가천대경원, 가천대인천,건국대, 경상대, 경희대
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            //string imit_realQuery = string.Format("select  link_code,idx,univ_code "
            //                                                   + "    kor_code, kor_ori, kor_std, kor_per, kor_gra,"
            //                                                   + "    math_code, mat_ori, mat_std, mat_per, mat_gra,"
            //                                                   + "    eng_code, eng_ori, eng_std, eng_per, eng_gra,"
            //                                                   + "    tam1_code, tam1_name, tam1_ori, tam1_std, tam1_per, tam1_gra,"
            //                                                   + "    tam2_code, tam2_name, tam2_ori, tam2_std, tam2_per, tam2_gra,"
            //                                                   + "    lang2_code, lang2_name, lang2_ori, lang2_std, lang2_per, lang2_gra,"
            //                                                   + "    his_grade"
            //                                            + " from {0} as a", config.CS06_MYJIWON_JUNGSI_IMIT_REAL);



            //임시 한국사 null인것만
            //string imit_realQuery = string.Empty;
            //string imit_realQuery = string.Format("select * from {0} {1} where univ_name in('성균관대','한양대','중앙대') order by idx", imitTableName,whereQuery);
            //string imit_realQuery = string.Format("select * from {0} where univ_code in('047','063','071') order by idx", imitTableName);
            //string imit_realQuery = string.Format("select * from {0} where univ_code in('315','951','701','015','090','301','313','419','433','601','713') and idx='6951' order by idx", imitTableName);
            string imit_realQuery = string.Format("select * from {0} {1}  order by idx", imitTableName, whereQuery);


            if (!string.IsNullOrEmpty(univName) && string.IsNullOrEmpty(linkCd))
            {
                imit_realQuery = string.Format("select  * from {0} where univ_code='{1}'", imitTableName, univName);
            }

            if (string.IsNullOrEmpty(univName) && !string.IsNullOrEmpty(linkCd))
            {
                imit_realQuery = string.Format("select  * from {0} where link_code='{1}'", imitTableName, linkCd);
            }

            if (!string.IsNullOrEmpty(univName) && !string.IsNullOrEmpty(linkCd))
            {
                imit_realQuery = string.Format("select  * from {0} where link_code='{1}' and univ_code='{2}'", imitTableName, linkCd, univName);
            }

            //테스트용
           //imit_realQuery = string.Format("select  * from where idx='4002' {0}", imitTableName);



            string masterQuery = string.Format("SELECT a.*, a.total_score1 * a.sung_aper1 / 100 AS SUNG_TOT1, a.total_score2 * a.sung_aper2 / 100 AS SUNG_TOT2, a.tam_subject_num AS tam_cnt"
                    + " FROM {0} as a"
                    
                    , config.CS06_MASTER4);


            DataTable mstDt = Proc.CreateCommandText(masterQuery);

            mstDt.Columns.Add("math_aper", typeof(float));                     // 수리영역 반영비율
            mstDt.Columns.Add("tam_aper", typeof(float));                      // 탐구영역 반영비율
            mstDt.Columns.Add("kor_my_up_score1", typeof(float));              // 탐구영역 반영비율
            mstDt.Columns.Add("math_my_up_score1", typeof(float));             // 탐구영역 반영비율
            mstDt.Columns.Add("eng_my_up_score1", typeof(float));              // 탐구영역 반영비율
            mstDt.Columns.Add("tam_my_up_score1", typeof(float));              // 탐구영역 반영비율
            mstDt.Columns.Add("lang2_my_up_score1", typeof(float));            // 탐구영역 반영비율                                                                                           //dt.Columns.Add("tam_cnt", typeof(float));              // 탐구영역 반영비율
            mstDt.Columns.Add("kor_aper", typeof(float));                      // 언어 반영비율
            mstDt.Columns.Add("eng_aper", typeof(float));                      // 외국어 반영비율  
            mstDt.Columns.Add("engPerManjum", typeof(bool));
            mstDt.Columns.Add("engGraAddScore", typeof(bool));
            mstDt.Columns.Add("korHisGraAddScore", typeof(bool));
            //mstDt.Columns.Add("sung_remark", typeof(string));


            DataTable imitDt = Proc.CreateCommandText(imit_realQuery);
            DataTable rstDt = new DataTable();
            DataTable gradeDt = Proc.CreateCommandText(string.Format("select * from  {0}", config.CS06_GRADE_TABLE_JUNGSI));
            DataTable partDt = Proc.CreateCommandText(string.Format("select * from {0}", config.CS06_PART_DIV_NEW));
            //기존산출로직내신객체
            NsScore ftNsScore = CalcNs.GetFtNsScore();
            NsNonScore nsNonScore = CalcNs.GetFtNsNonFromSql(config.UsrId_UnitCode1, config.StuUseYear);
            UserInfo usrInfo = new UserInfo
            {
                StuId = config.UsrId_UnitCode1,
                ScoHak = "3",
                StuUseYear = config.StuUseYear,
                TestDate = config.SnTestDate,
                TopId = null,
                StuGrade = config.StuUseYear,
            };
            string resultContents = string.Empty;
            string errContents = string.Empty;

            string printContents = string.Empty;
            foreach (DataRow imitDr in imitDt.Rows)
            {
                SnScore sn = GetAverageScore2019(imitDr, config.TestDate);

                DataRow mstDr = null;
                try
                {
                    //DataRow[] tempDrs = mstDt.Select(string.Format("apply_score_s='{0}' and univ_code='{1}'", imitDr["link_code"].ToString(), imitDr["univ_code"].ToString()));
                    //if (tempDrs.Count() > 1)
                    //{
                    //    mstDr = mstDt.Select(string.Format("apply_score_s='{0}' and univ_code='{1}' ", imitDr["link_code"].ToString(), imitDr["univ_code"].ToString()))[0];
                    //}
                    //else
                    //{

                    //}
                    mstDr = mstDt.Select(string.Format("apply_score_s='{0}' and univ_code='{1}' and unit_code='{2}'", imitDr["link_code"].ToString(), imitDr["univ_code"].ToString(), imitDr["unit_code"].ToString()))[0];


                }
                catch
                {
                    continue;
                }
                NSCalc master = new NSCalc
                {
                    StypeRem = mstDr["stype_rem"].ToString(),
                    UnivCode = mstDr["univ_code"].ToString(),
                    MajorCode = mstDr["major_code"].ToString(),
                    RtimeCode = mstDr["rtime_code"].ToString(),
                    StypeCode = mstDr["stype_code"].ToString(),
                };

                try
                {
                    //DataRow resultDr = CollegeScholasticAbilityTest.CalculateCSAT(usrInfo, master, sn, ftNsScore, nsNonScore);
                    //rstDt.ImportRow(resultDr);
                    //UnivScoreResult usr = new UnivScoreResult(resultDr);
                    MasterExtention m = SetAper(mstDr);
                    mstDr["kor_aper"] = m.KorAper.ToString();
                    mstDr["eng_Aper"] = m.EngAper.ToString();
                    mstDr["tam_Aper"] = m.TamAper.ToString();
                    mstDr["math_aper"] = m.MathAper.ToString();
                    mstDr["his_Aper"] = m.HisAper.ToString();
                    mstDr["lang2_Aper"] = m.Lang2Aper.ToString();

                    UnivScore us1 = null;
                    UnivSnExJungSi usnex = null;
                    BasicConnection univCon = null;
                    univCon = new WebConnection();
                    usnex = usnex = UnivSelector.SelSn(Parser.ObjToStr(mstDr["univ_name"]), mstDr, univCon);
                    usnex.PreSet(mstDr, sn);
                    usnex.Cs06_grade_table = gradeDt;
                    UnivParameter uniParam = CollegeScholasticAbilityTest.SetUnivParameter(sn, mstDr["score_type"].ToString(), true);
                    us1 = usnex.GetUnivScore(mstDr, Parser.ObjToFloat(mstDr["SUNG_TOT1"]), Parser.ObjToFloat(mstDr["total_score1"]),
                            uniParam.kor, uniParam.math, uniParam.engGra, uniParam.tam, uniParam.tam1, uniParam.tam2, 0, 0, 0, sn, 1);

                    mstDr["kor_apply_score"] = us1.kor;
                    mstDr["math_apply_score"] = us1.math;
                    mstDr["eng_apply_score"] = us1.eng;
                    mstDr["tam_apply_score"] = us1.tam;
                    mstDr["lang2_apply_score"] = us1.lang2;
                    mstDr["his_apply_score"] = us1.korHis;

                    if (us1.kor + us1.math + us1.tam <= 0f)
                    {
                        us1.upScore = 0;
                        us1.kor = 0;
                        us1.math = 0;
                        us1.tam = 0;
                        us1.eng = 0;
                        us1.korHis = 0;
                        us1.lang2 = 0;
                        us1.AddScores.KorAdd = 0;
                        us1.AddScores.MathAdd = 0;
                        us1.AddScores.TamAdd = 0;
                        us1.AddScores.EngAdd = 0;
                        us1.AddScores.KorHisAdd = 0;
                        us1.AddScores.Lang2Add = 0;
                    }
                    if (us1 != null)
                    {
                        float maxTotalScore = 0;
                        string maxPartName = string.Empty;
                        foreach (DataRow partDr in partDt.Select(string.Format("part_method_code='{0}'", mstDr["part_method_code"])))
                        {                            
                            string partName = partDr["part_name"].ToString();
                            float total_score = 0;
                            if (partName.IndexOf("언") >= 0 || partName.IndexOf("국") >= 0)
                            {
                                total_score += (us1.kor + us1.AddScores.KorAdd);
                            }
                            if (partName.IndexOf("수") >= 0)
                            {
                                total_score += (us1.math + us1.AddScores.MathAdd);
                            }
                            if (partName.IndexOf("외") >= 0 || partName.IndexOf("영") >= 0)
                            {
                                total_score += (us1.eng);
                            }
                            if (partName.IndexOf("사") >= 0 || partName.IndexOf("과") >= 0 || partName.IndexOf("탐") >= 0)
                            {
                                total_score += (us1.tam + us1.AddScores.TamAdd);
                            }
                            if (partName.IndexOf("한") >= 0)
                            {
                                total_score += (us1.korHis);
                            }

                            if (maxTotalScore < total_score)
                            {
                                maxTotalScore = total_score;
                                maxPartName = partName;
                            }
                        }

                        if (maxPartName.IndexOf("언") == -1 && maxPartName.IndexOf("국") == -1)
                        {
                            us1.kor = 0;
                            us1.AddScores.KorAdd = 0;
                        }
                        if (maxPartName.IndexOf("수") == -1)
                        {
                            us1.math = 0;
                            us1.AddScores.MathAdd = 0;
                        }
                        if (maxPartName.IndexOf("외") == -1 && maxPartName.IndexOf("영") == -1)
                        {
                            us1.eng = 0;
                            //if(!(mstDr["cross_add_score1"].ToString() == "2147" || 
                            //   mstDr["cross_add_score1"].ToString() == "1014" ||
                            //   mstDr["cross_add_score1"].ToString() == "1125" ||
                            //   mstDr["cross_add_score1"].ToString() == "1140" ||
                            //   mstDr["cross_add_score1"].ToString() == "1123" ||
                            //   mstDr["cross_add_score1"].ToString() == "1115" ||
                            //   mstDr["cross_add_score1"].ToString() == "2180" ||
                            //   mstDr["cross_add_score1"].ToString() == "1107" ||
                            //   mstDr["cross_add_score1"].ToString() == "2140" ||
                            //   mstDr["cross_add_score1"].ToString() == "1124" ||
                            //   mstDr["cross_add_score1"].ToString() == "1164" ||
                            //   mstDr["cross_add_score1"].ToString() == "1118" ||
                            //   mstDr["cross_add_score1"].ToString() == "1141" ||
                            //   mstDr["cross_add_score1"].ToString() == "1122" ||
                            //   mstDr["cross_add_score1"].ToString() == "1116"))
                            //{
                            //    us1.AddScores.EngAdd = 0;
                            //}
                            
                        }
                        if (maxPartName.IndexOf("사") == -1 && maxPartName.IndexOf("과") == -1 && maxPartName.IndexOf("탐") == -1)
                        {
                            us1.tam = 0;
                            us1.AddScores.TamAdd = 0;
                        }
                        if (maxPartName.IndexOf("한") == -1)
                        {
                            us1.korHis = 0;
                            //us1.AddScores.KorHisAdd = 0;
                        }
                    }

                    
                    us1.upScore = us1.kor + us1.math + us1.eng + us1.tam + us1.korHis;
                    float totalScore = us1.upScore +
                                   us1.AddScores.KorAdd +
                                   us1.AddScores.MathAdd +
                                   us1.AddScores.TamAdd +
                                   us1.AddScores.EngAdd +
                                   us1.AddScores.KorHisAdd + us1.AddScores.Lang2Add;

                    UnivScoreResult univScoreResult = new UnivScoreResult();
                    univScoreResult.MySumScore = totalScore.ToString();
                    univScoreResult.upScore = us1.upScore;
                    univScoreResult.kor = us1.kor;
                    univScoreResult.math = us1.math;
                    univScoreResult.tam = us1.tam;
                    univScoreResult.eng = us1.eng;
                    univScoreResult.korHis = us1.korHis;
                    univScoreResult.lang2 = us1.lang2;
                    univScoreResult.AddScores.KorAdd = us1.AddScores.KorAdd;
                    univScoreResult.AddScores.MathAdd = us1.AddScores.MathAdd;
                    univScoreResult.AddScores.TamAdd = us1.AddScores.TamAdd;
                    univScoreResult.AddScores.EngAdd = us1.AddScores.EngAdd;
                    univScoreResult.AddScores.KorHisAdd = us1.AddScores.KorHisAdd;
                    univScoreResult.AddScores.Lang2Add = us1.AddScores.Lang2Add;
                    univScoreResult.MasterInfo.idx = Convert.ToInt32(mstDr["idx"].ToString());
                    univScoreResult.MasterInfo.univ_name = mstDr["univ_name"].ToString();
                    univScoreResult.MasterInfo.major_name = mstDr["major_name"].ToString();
                    univScoreResult.MasterInfo.univ_area = mstDr["univ_area"].ToString();
                    univScoreResult.MasterInfo.major_name = mstDr["major_name"].ToString();
                    univScoreResult.MasterInfo.stype_rem = mstDr["stype_rem"].ToString();
                    univScoreResult.MasterInfo.part_name = mstDr["part_name"].ToString();
                    univScoreResult.MasterInfo.score_type = Convert.ToInt32(mstDr["score_type"].ToString());

                    string context = string.Empty;
                    context = "_" + " : "
      + "IDX:" + imitDr["idx"].ToString()
      + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
      //+ "\r\n" + "학과명:" + mstDr["major_name"].ToString()
      //+ "\r\n" + "전형명:" + mstDr["stype_rem"].ToString()
      + "\r\n" + "총산출점수:" + univScoreResult.MySumScore
      + "\r\n" + "-------------------------------------------------------------";



                    resultContents += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\r\n"
                        , imitDr["idx"].ToString()
                        , mstDr["univ_name"].ToString()
                        , mstDr["univ_code"].ToString()
                        , mstDr["apply_score_s"].ToString()
                        , mstDr["part_code"].ToString()
                        , mstDr["part_name"].ToString()
                        , mstDr["tam_subject_num"].ToString()
                        , mstDr["score_type"].ToString()
                        , univScoreResult.MySumScore
                        , univScoreResult.kor
                        , univScoreResult.math
                        , univScoreResult.tam
                        , univScoreResult.eng
                        , univScoreResult.korHis
                        , univScoreResult.lang2
                        , univScoreResult.AddScores.KorAdd 
                        , univScoreResult.AddScores.MathAdd 
                        , univScoreResult.AddScores.TamAdd
                        , univScoreResult.AddScores.EngAdd 
                        , univScoreResult.AddScores.KorHisAdd 
                        , univScoreResult.AddScores.Lang2Add
                        , uniParam.kor
                        , uniParam.math
                        , uniParam.engGra
                        , uniParam.tam
                        , uniParam.tam1
                        , uniParam.tam2);
                    printContents += context;
                    Console.WriteLine(context);
                }
                catch (Exception EX)
                {
                    string context = "IDX:" + imitDr["idx"].ToString() + "\r\n"
                             + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                             + "\r\n" + "에러내용:" + EX.StackTrace;
                    Console.WriteLine(context);
                    resultContents += string.Format("{0}\t{1}\t{2}\r\n", imitDr["idx"].ToString()
                        , mstDr["univ_name"].ToString()
                        , EX.StackTrace);
                }
            }

            //string fileName = string.Format(@"{0}\{1}.txt", config.Directory, imitTableName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm"));
            string fileName = string.Format(@"{0}.txt", imitTableName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm"));
            System.IO.File.WriteAllText(fileName, resultContents, Encoding.Default);

            fileName = string.Format(@"{0}.txt", "Err" + imitTableName);
            System.IO.File.WriteAllText(fileName, errContents, Encoding.Default);
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
        }

        public MasterExtention SetAper(DataRow dr)
        {
            DataRow drMaster = dr;
            MasterExtention m = new MasterExtention();

            m.Kor_a_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["kora_aper"]));
            m.Kor_b_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["korb_aper"]));
            m.Kor_b_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["korab_aper"]));
            m.KorAper = m.Kor_a_Aper + m.Kor_b_Aper + m.Kor_ab_Aper;

            m.Math_a_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["matha_Aper"]));
            m.Math_b_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["mathb_Aper"]));
            m.Math_ab_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["mathab_Aper"]));
            m.MathAper = m.Math_a_Aper + m.Math_b_Aper + m.Math_ab_Aper;

            m.Eng_a_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["enga_aper"]));
            m.Eng_b_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["engb_aper"]));
            m.Eng_ab_Aper = Math.Abs(Parser.ObjToFloatA(drMaster["engab_aper"]));
            m.EngAper = m.Eng_a_Aper + m.Eng_b_Aper + m.Eng_ab_Aper;

            m.SocAper = Math.Abs(Parser.ObjToFloatA(drMaster["soc_aper"]));
            m.SciAper = Math.Abs(Parser.ObjToFloatA(drMaster["sci_aper"]));
            m.JobAper = Math.Abs(Parser.ObjToFloatA(drMaster["job_aper"]));
            m.SocSciAper = Math.Abs(Parser.ObjToFloatA(drMaster["socsci_aper"]));
            m.SocJobAper = Math.Abs(Parser.ObjToFloatA(drMaster["socjob_aper"]));
            m.SciJobAper = Math.Abs(Parser.ObjToFloatA(drMaster["scijob_aper"]));
            m.SocSciJobAper = Math.Abs(Parser.ObjToFloatA(drMaster["socscijob_aper"]));
            //m.TamAper = 0.0f;

            m.TamAper = m.SocAper + m.SciAper + m.JobAper + m.SocSciAper + m.SocJobAper + m.SocSciJobAper;
            m.Lang2Aper = Math.Abs(Parser.ObjToFloatA(drMaster["lang2_aper"]));
            m.HisAper = Math.Abs(Parser.ObjToFloatA(drMaster["his_aper"]));
            return m;
        }
        private SnScore GetAverageScore(DataRow dr, string testDate)
        {

            SnScore avrgRecord = new SnScore(new WebConnection());
            avrgRecord.KorCode = "001";
            avrgRecord.StdKor = dr["sc_kor_std"].ToString();
            avrgRecord.PerKor = dr["sc_kor_per"].ToString();
            avrgRecord.GraKor = dr["sc_kor_gra"].ToString();

            avrgRecord.MatName = dr["math_code"].ToString().Equals("002") ? "가형" : "나형";
            avrgRecord.MatCode = dr["math_code"].ToString();
            avrgRecord.StdMat = dr["sc_mat_std"].ToString();
            avrgRecord.PerMat = dr["sc_mat_per"].ToString();
            avrgRecord.GraMat = dr["sc_mat_gra"].ToString();

            avrgRecord.EngCode = dr["eng_code"].ToString();
            avrgRecord.GraEng = dr["eng_gra"].ToString();

            avrgRecord.Sech1Code = dr["tam1_code"].ToString();
            avrgRecord.Sech1Name = dr["tam1_name"].ToString();
            avrgRecord.StdSech1 = dr["sc_tam1_std"].ToString();
            avrgRecord.PerSech1 = dr["sc_tam1_per"].ToString();
            avrgRecord.GraSech1 = dr["sc_tam1_gra"].ToString();

            avrgRecord.Sech2Code = dr["tam2_code"].ToString();
            avrgRecord.Sech2Name = dr["tam2_name"].ToString();
            avrgRecord.StdSech2 = dr["sc_tam2_std"].ToString();
            avrgRecord.PerSech2 = dr["sc_tam2_per"].ToString();
            avrgRecord.GraSech2 = dr["sc_tam2_gra"].ToString();

            //한국사가 없거나 잘못들어가면 3으로 치환
            avrgRecord.GraKorHis = dr["his_grade"].ToString();
            if (Parser.ObjToIntA(avrgRecord.GraKorHis) <= 0)
                avrgRecord.GraKorHis = "3";

            avrgRecord.Lang2Code = dr["lang2_code"].ToString();
            avrgRecord.Lang2Name = dr["lang2_name"].ToString();
            avrgRecord.StdLang2 = dr["sc_lang2_std"].ToString();
            avrgRecord.PerLang2 = dr["sc_lang2_per"].ToString();
            avrgRecord.GraLang2 = dr["sc_lang2_gra"].ToString();

            //avrgRecord.StdKor = dr["kor_std"].ToString();
            //avrgRecord.PerKor = dr["kor_per"].ToString();
            //avrgRecord.GraKor = dr["kor_gra"].ToString();

            //avrgRecord.MatName = dr["math_code"].ToString().Equals("002") ? "가형" : "나형";
            //avrgRecord.MatCode = dr["math_code"].ToString();
            //avrgRecord.StdMat = dr["mat_std"].ToString();
            //avrgRecord.PerMat = dr["mat_per"].ToString();
            //avrgRecord.GraMat = dr["mat_gra"].ToString();

            //avrgRecord.EngCode = dr["eng_code"].ToString();
            //avrgRecord.GraEng = dr["eng_gra"].ToString();

            //avrgRecord.Sech1Code = dr["tam1_code"].ToString();
            //avrgRecord.Sech1Name = dr["tam1_name"].ToString();
            //avrgRecord.StdSech1 = dr["tam1_std"].ToString();
            //avrgRecord.PerSech1 = dr["tam1_per"].ToString();
            //avrgRecord.GraSech1 = dr["tam1_gra"].ToString();

            //avrgRecord.Sech2Code = dr["tam2_code"].ToString();
            //avrgRecord.Sech2Name = dr["tam2_name"].ToString();
            //avrgRecord.StdSech2 = dr["tam2_std"].ToString();
            //avrgRecord.PerSech2 = dr["tam2_per"].ToString();
            //avrgRecord.GraSech2 = dr["tam2_gra"].ToString();

            //avrgRecord.GraKorHis = dr["his_grade"].ToString();

            //avrgRecord.Lang2Code = dr["lang2_code"].ToString();
            //avrgRecord.Lang2Name = dr["lang2_name"].ToString();
            //avrgRecord.StdLang2 = dr["lang2_std"].ToString();
            //avrgRecord.PerLang2 = dr["lang2_per"].ToString();
            //avrgRecord.GraLang2 = dr["lang2_gra"].ToString();
            avrgRecord.TestDate = testDate;

            return avrgRecord;
        }

        private SnScore GetAverageScore2019(DataRow dr, string testDate)
        {

            SnScore avrgRecord = new SnScore(new WebConnection());
            avrgRecord.KorCode = "001";
            avrgRecord.StdKor = dr["k_std"].ToString();
            avrgRecord.PerKor = dr["k_per"].ToString();
            avrgRecord.GraKor = dr["k_grade"].ToString();

            avrgRecord.MatName = dr["m_name"].ToString().Equals("수학(가형)") ? "가형" : "나형";
            avrgRecord.MatCode = dr["m_name"].ToString().Equals("수학(가형)") ? "002" : "003";
            avrgRecord.StdMat = dr["m_std"].ToString();
            avrgRecord.PerMat = dr["m_per"].ToString();
            avrgRecord.GraMat = dr["m_grade"].ToString();

            avrgRecord.EngCode = "004";
            avrgRecord.GraEng = dr["e_grade"].ToString();

            avrgRecord.Sech1Code = dr["t1_code"].ToString();
            avrgRecord.Sech1Name = dr["t1_name"].ToString();
            avrgRecord.StdSech1 = dr["t1_std"].ToString();
            avrgRecord.PerSech1 = dr["t1_per"].ToString();
            avrgRecord.GraSech1 = dr["t1_grade"].ToString();
            
            avrgRecord.Sech2Code = dr["t2_code"].ToString();
            avrgRecord.Sech2Name = dr["t2_name"].ToString();
            avrgRecord.StdSech2 = dr["t2_std"].ToString();
            avrgRecord.PerSech2 = dr["t2_per"].ToString();
            avrgRecord.GraSech2 = dr["t2_grade"].ToString();

            //한국사가 없거나 잘못들어가면 3으로 치환
            avrgRecord.GraKorHis = dr["his_grade"].ToString();
            if (Parser.ObjToIntA(avrgRecord.GraKorHis) <= 0)
                avrgRecord.GraKorHis = "3";

            avrgRecord.Lang2Code = dr["e2_code"].ToString();
            avrgRecord.Lang2Name = dr["e2_name"].ToString();
            avrgRecord.StdLang2 = dr["e2_std"].ToString();
            avrgRecord.PerLang2 = dr["e2_per"].ToString();
            avrgRecord.GraLang2 = dr["e2_grade"].ToString();

            avrgRecord.SechCheck = dr["m_name"].ToString().Equals("수학(가형)") ? "2" : "1";
            //avrgRecord.StdKor = dr["kor_std"].ToString();
            //avrgRecord.PerKor = dr["kor_per"].ToString();
            //avrgRecord.GraKor = dr["kor_gra"].ToString();

            //avrgRecord.MatName = dr["math_code"].ToString().Equals("002") ? "가형" : "나형";
            //avrgRecord.MatCode = dr["math_code"].ToString();
            //avrgRecord.StdMat = dr["mat_std"].ToString();
            //avrgRecord.PerMat = dr["mat_per"].ToString();
            //avrgRecord.GraMat = dr["mat_gra"].ToString();

            //avrgRecord.EngCode = dr["eng_code"].ToString();
            //avrgRecord.GraEng = dr["eng_gra"].ToString();

            //avrgRecord.Sech1Code = dr["tam1_code"].ToString();
            //avrgRecord.Sech1Name = dr["tam1_name"].ToString();
            //avrgRecord.StdSech1 = dr["tam1_std"].ToString();
            //avrgRecord.PerSech1 = dr["tam1_per"].ToString();
            //avrgRecord.GraSech1 = dr["tam1_gra"].ToString();

            //avrgRecord.Sech2Code = dr["tam2_code"].ToString();
            //avrgRecord.Sech2Name = dr["tam2_name"].ToString();
            //avrgRecord.StdSech2 = dr["tam2_std"].ToString();
            //avrgRecord.PerSech2 = dr["tam2_per"].ToString();
            //avrgRecord.GraSech2 = dr["tam2_gra"].ToString();

            //avrgRecord.GraKorHis = dr["his_grade"].ToString();

            //avrgRecord.Lang2Code = dr["lang2_code"].ToString();
            //avrgRecord.Lang2Name = dr["lang2_name"].ToString();
            //avrgRecord.StdLang2 = dr["lang2_std"].ToString();
            //avrgRecord.PerLang2 = dr["lang2_per"].ToString();
            //avrgRecord.GraLang2 = dr["lang2_gra"].ToString();
            avrgRecord.TestDate = testDate;

            return avrgRecord;
        }

        //private void btnGetApplySnScore_Click(object sender, EventArgs e)
        //{
        //    string query = DefaultFns.SetMasterQueryByIdx(config.CS06_MASTER4, "2949");
        //    DataTable dt = DefaultFns.QueryMaster(query, true);
        //    //수능성적조회
        //    SnScore sn = CollegeScholasticAbilityTest.SetSnScore("DZK00044", "20191114_0");
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        float result = 0f;
        //        result = DefaultFns.배치성적구하기(dr, Parser.ObjToFloatA(dr["tam_subject_num"]), sn);

        //        MessageBox.Show(result.ToString());
        //    }
        //}

        private void TestUnivCalc()
        {
            Config.TABLE_SCORECP = "CS06_SCORE_CP";
            Config.TABLE_SCORE = "CS06_SCORE_CP";
            MainApp.Score.NsScore nsScores = CalcNs.GetFtNsFromSql(config.UsrId_UnitCode1, config.StuUseYear);
            MainApp.Score.SnScore snScores = CalcNs.GetSnScoreFromSql(config.UsrId_UnitCode1, config.TestDate);
            MainApp.Score.NsNonScore nsNonScores = CalcNs.GetFtNsNonFromSql(config.UsrId_UnitCode1, config.StuUseYear);
            WebConnection webConnection = new WebConnection(true);
            webConnection.IsUseConnectionConfig = false;
            UnivSn_J univSn_J = new UnivSn_J(webConnection);
            DataTable dt = univSn_J.Search(config.UsrId_UnitCode1,
                    "3",
                    config.StuUseYear,
                    config.TestDate,
                    null,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0,
                    0,
                    0,
                    "",
                    false,
                    "",
                    "",
                    0,
                    0,
                    false,
                    true,
                    false,
                    false,
                    "AND M4.UNIV_AREA ='01' AND M4.UNIV_CODE = '103' AND M4.UNIT_CODE = '1' AND M4.RTIME_CODE = '41' AND SUNG_APER1>0",
                    null,
                    config.StuUseYear,
                    false,
                    false,
                    null,
                    "",
                    1,
                    
                    true,
                    null);

        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            TestUnivCalc();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = Proc.CreateCommandText(string.Format("select * from {0} where univ_name like '{1}%'", config.CS06_MASTER4, this.txtSUnivNm.Text));
            string univCd = dt.Rows[0]["univ_code"].ToString();
            string univNm = dt.Rows[0]["univ_name"].ToString();
            this.txtUnivName.Text = univCd;
            this.lblUnivCd.Text = univNm + "/" + univCd;
        }

        //private void btnTest_Click_1(object sender, EventArgs e)
        //{
        //    string query = DefaultFns.SetMasterQueryByIdx(config.CS06_MASTER4, "2955");
        //    DataTable dt = DefaultFns.QueryMaster(query, true);
        //    //수능성적조회
        //    SnScore sn = CollegeScholasticAbilityTest.SetSnScore("DZK00044", "20191114_0");
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        float result = 0f;
        //        result = DefaultFns.배치성적구하기(dr, Parser.ObjToFloatA(dr["tam_subject_num"]), sn);

        //        MessageBox.Show(result.ToString());
        //    }
        //}
    }
    public enum TestType { 전문대_정시지원가능점수, 정시지원가능점수, 수시지원가능점수, 정시학생점수확인, 수시학생점수확인, 정시학생부지원가능점수, 입결정시산출점수, 정시내신점수확인, 정시타사배치점수 }

    public class DefaultSettingValues
    {
        //public string CS06_MYJIWON_JUNGSI_IMIT_REAL = "futureplan_moi.dbo.CS06_MYJIWON_JUNGSI_IMIT_REAL_2017";
        public string CS06_MYJIWON_JUNGSI_IMIT_REAL = "FPmaster.dbo.CS06_MYJIWON_JUNGSI_IMIT_REAL_2018_COMPACT_V3";
        public string CS06_MASTER4 = "dbo.cs06_master4_19";
        public string CS06_MASTER2 = "dbo.cs06_master2_19";
        public string CS06_GRADE_TABLE_JUNGSI = "CS06_GRADE_TABLE_JUNGSI_19";
        public string CS06_PART_DIV_NEW = "CS06_PART_DIV_NEW";

        public string Directory = @"D:\정시산출";
        public string UsrId_UnitCode1 = "301002";
        public string UsrId_UnitCode2 = "301001";        
        public string StuUseYear = "2019";

        /// <summary>
        /// 정시산출점수 기준날짜
        /// </summary>
        public string SnTestDate = "20191114_0";
        public string TestDate = "20191114_0";
    }
}
