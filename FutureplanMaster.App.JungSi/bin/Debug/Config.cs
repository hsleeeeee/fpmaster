﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MainApp.Common
{
    public class Config
    {
		#region 기초 설정

		/// <summary>
		///  2012로 되어 있음
		/// </summary>
		public static int CURRENT_YEAR = 2019;//DateTime.Now.ToLocalTime().Year;

        // UnivSn에 쓰이는 part_div의 db조회수를 줄이기위해 static로 넣는다.
        public static System.Data.DataTable DataTable_dt_part_div;
        public static System.Data.DataTable DataTable_dt_part_div_new;

        #endregion

        #region 테이블 이름 설정

        public static String TABLE_NESIN = "CS06_NESIN";
        public static String TABLE_NESIN_SUMMARY = "CS06_NESIN_SUMMARY";
        public static String TABLE_NESINCP = "CS06_NESIN";
        public static String TABLE_NESIN_SUMMARYCP = "CS06_NESIN_SUMMARY";
        public static String TABLE_NESIN_V = "CS06_NESIN_V";
        public static String TABLE_NESIN_NON = "CS06_NESIN_NON";
        public static String TABLE_NESIN_NON_2017 = "CS06_NESIN_NON_2017";
        public static String TABLE_NESIN_NONCP = "CS06_NESIN_NON";
        public static String TABLE_SCORE = "CS06_SCORE";
        public static String TABLE_SCORECP = "CS06_SCORE";
        public static String TABLE_HPUNIV = "CS06_HPUNIV";
        public static String TABLE_INTMAJOR = "CS06_INTMAJOR";
        public static String TABLE_TEST_STU_SEL = "CS06_TEST_STU_SEL";
        public static String TABLE_TEST_STUCHECK = "CS06_TEST_STUCHECK";
        public static String TABLE_STU_MEMBER = "CS06_STU_MEMBER";
        public static String TABLE_TEA_MEMBER = "CS06_TEACHER_MEMBER";
        public static String TABLE_STU_LIST = "CS06_STU_LIST";
        public static String TABLE_HIGH_SCHOOL = "CS06_HIGH_SCHOOL";
        public static String TABLE_STU_CLASS = "CS06_STU_CLASS";
        public static String TABLE_STU_HIGH = "CS06_STU_HIGH";
        public static String TABLE_PART_APP = "CS06_PART_APP";
        //public static String TABLE_NESIN_APP = "CS06_NESIN_APP"; // 디버깅완료(이상직)
        public static String TABLE_SCORE_PER = "CS06_SCORE_PER";
        public static String TABLE_SMASTER4 = "CS06_SMASTER4";
        public static String TABLE_SMASTER4_07 = "CS06_SMASTER4_07"; // 디버깅완료(이상직)
        public static String TABLE_SMASTER2 = "CS06_SMASTER2"; // 디버깅완료(이상직)
        public static String TABLE_MASTER2 = "CS06_MASTER2"; // 디버깅완료(이상직)
        public static String TABLE_NESININFO4 = "CS06_NESININFO4"; // 디버깅완료(이상직)
        public static String VIEW_MASTER4_PCS_V = "CS06_MASTER4_PCS_V"; // 디버깅완료(이상직)
        public static String TABLE_GRADE_COMPARE = "CS06_GRADE_COMPARE";
        public static String TABLE_COM = "CS06_COM";
        public static String TABLE_WISH_UNIVNS4 = "CS_WISH_UNIVNS4"; // 디버깅완료(이상직)
        public static String TABLE_PART_DIV = "CS06_PART_DIV";
        public static String TABLE_PART_DIV_NEW = "CS06_PART_DIV_NEW";
        public static String TABLE_ACADEMY_ROOM = "CS06_ACADEMY_ROOM";
        public static String TABLE_ACADEMY = "CS06_ACADEMY";
        public static String TABLE_APTITUDE_TEST = "CS06_APTITUDE_TEST";

        public static String TABLE_MYJIWON_UNION = "CS06_MYJIWON_JUNGSI_UNION"; // VIEW
        public static String TABLE_MYJIWON_UNION_SUSI = "CS06_MYJIWON_SUSI_UNION"; // VIEW
        public static String TABLE_MYJIWON = "CS06_MYJIWON_JUNGSI";
        public static String TABLE_UNIVINFO = "CS06_UNIVINFO";
        public static String TABLE_CONSULT = "CS06_CONSULT";
        public static String TABLE_ADVICE = "CS06_ADVICE";
        public static String TABLE_POST = "CS06_POST";
        public static String TABLE_STATISTIC_JOIN_LOG = "CS06_STATISTIC_JOIN_LOG"; // 통계처리참여로그

        // 비교과
        public static String TABLE_POL_AWARD = "CS06_PORTFOLIO";                // 수상경력
        public static String TABLE_POL_LICENSE = "CS06_PORTFOLIO";            // 자격증및인증
        public static String TABLE_POL_LEADER = "CS06_PORTFOLIO";              // 임원활동
        public static String TABLE_POL_CLUB = "CS06_PORTFOLIO";                  // 동아리활동
        public static String TABLE_POL_SERVICE = "CS06_PORTFOLIO";            // 봉사활동
        public static String TABLE_POL_FREE = "CS06_PORTFOLIO";                  // 자율활동
        public static String TABLE_POL_EXP = "CS06_PORTFOLIO";                    // 체험활동
        public static String TABLE_POL_AFTERSCHOOL = "CS06_PORTFOLIO ";   // 방과후활동
        public static String TABLE_POL_READING = "CS06_PORTFOLIO";                // 독서활동


        /// <summary>
        /// 입시플래너 - 웹 연동 세션 저장 테이블
        /// </summary>
        public static String TABLE_IPLSESSION = "CS06_IPLSESSION";

        /* 실제 용*/
        public static String TABLE_AVRG = "CS06_AVRG";
        public static String TABLE_MASTER4 = "CS06_MASTER4";
        public static String TABLE_MASTER4_08 = "CS06_MASTER4_J";
        public static String TABLE_MASTER2_08 = "CS06_MASTER2_J";
        public static String TABLE_MASTER2_09 = "CS06_MASTER2_09";
        public static String TABLE_MASTER2_10 = "CS06_MASTER2_10";
        public static String TABLE_MASTER2_11 = "CS06_MASTER2_11";
        public static String TABLE_MASTER2_12 = "CS06_MASTER2_12";
        public static String TABLE_MASTER2_13 = "CS06_MASTER2_13";
        public static String TABLE_MASTER2_14 = "CS06_MASTER2_14";
        public static String TABLE_MASTER2_15 = "CS06_MASTER2_15";
        public static String TABLE_MASTER2_16 = "CS06_MASTER2_16";
        public static String TABLE_MASTER2_17 = "CS06_MASTER2_17";
        public static String TABLE_MASTER2_18 = "CS06_MASTER2_18";
		public static String TABLE_MASTER2_19 = "CS06_MASTER2_19";

		public static String TABLE_MASTER4_07 = "CS06_MASTER4_07"; // 디버깅완료(이상직)
        public static String TABLE_WISH_UNIV4 = "CS06_WISH_UNIV4";
        public static String TABLE_WISH_UNIV4_CP = "CS06_WISH_UNIV4";
        public static String TABLE_WISH_UNIV2 = "CS06_WISH_UNIV2";
        public static String TABLE_SU_RANK = "CS06_SU_RANK";
        public static String TABLE_LASTYEAR_RESULT = "CS06_LASTYEAR_RESULT";
        public static String TABLE_CMASTER2 = "CS06_CMASTER2"; // 디버깅완료(이상직)

        public static String TABLE_SCORE_CONVERT = "CS06_SCORE_CONVERT";
        public static String TABLE_GOAL_UNIV = "CS06_GOAL_UNIV";

        public static String TABLE_MASTER4_09 = "CS06_MASTER4_09";
        public static String TABLE_MASTER4_10 = "CS06_MASTER4_10";
        public static String TABLE_MASTER4_11 = "CS06_MASTER4_11";
        public static String TABLE_MASTER4_11_TIME = "CS06_MASTER4_11_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_11_JUNG = "CS06_MASTER4_11_JUNG"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_12 = "CS06_MASTER4_12";
        public static String TABLE_MASTER4_12_TIME = "CS06_MASTER4_11_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_12_JUNG = "CS06_MASTER4_11_JUNG"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_13 = "CS06_MASTER4_13";
        public static String TABLE_MASTER4_14 = "CS06_MASTER4_14";
        public static String TABLE_MASTER4_15 = "CS06_MASTER4_15";
        public static String TABLE_MASTER4_16 = "CS06_MASTER4_16";
        public static String TABLE_MASTER4_17 = "CS06_MASTER4_17";
        public static String TABLE_MASTER4_18 = "CS06_MASTER4_18";
		public static String TABLE_MASTER4_19 = "CS06_MASTER4_19";

		public static String TABLE_MASTER4_MOI_09 = "CS06_MASTER4_MOI_09";
        public static String TABLE_MASTER4_MOI_10 = "CS06_MASTER4_MOI_10";
        public static String TABLE_MASTER4_MOI_10_TIME = "CS06_MASTER4_MOI_10_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_MOI_11 = "CS06_MASTER4_MOI_11";
        public static String TABLE_MASTER4_MOI_11_TIME = "CS06_MASTER4_MOI_11_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_MOI_12 = "CS06_MASTER4_MOI_12";
        public static String TABLE_MASTER4_MOI_12_TIME = "CS06_MASTER4_MOI_12_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_MOI_13 = "CS06_MASTER4_MOI_13";
        public static String TABLE_MASTER4_MOI_13_TIME = "CS06_MASTER4_MOI_13_TIME"; // 디버깅완료(이상직)
        public static String TABLE_MASTER4_MOI_14 = "CS06_MASTER4_MOI_14";        
        public static String TABLE_MASTER4_MOI_14_TIME = "CS06_MASTER4_MOI_14_TIME"; // 디버깅완료(이상직) 
        public static String TABLE_MASTER4_MOI_15 = "CS06_MASTER4_MOI_15";
        public static String TABLE_MASTER4_MOI_15_TIME = "CS06_MASTER4_MOI_15_TIME";
        public static String TABLE_MASTER4_MOI_16 = "CS06_MASTER4_MOI_16";
        public static String TABLE_MASTER4_MOI_17 = "CS06_MASTER4_MOI_17";
        public static String TABLE_MASTER4_MOI_18 = "CS06_MASTER4_MOI_18";
		public static String TABLE_MASTER4_MOI_19 = "CS06_MASTER4_MOI_19";
		public static String TABLE_ORDER_TOT = "CS06_ORDER_TOT";

        public static String TABLE_WISH_UNIV_MAJOR = "CS06_WISH_UNIV_MAJOR";
        public static String TABLE_WISH_UNIV_MAJOR_CP = "CS06_WISH_UNIV_MAJOR";

        public static String TABLE_SMASTER4_09 = "CS06_SMASTER4_09";
        public static String TABLE_SMASTER4_10 = "CS06_SMASTER4_10";
        public static String TABLE_SMASTER4_11 = "CS06_SMASTER4_11";
        public static String TABLE_SMASTER4_12 = "CS06_SMASTER4_12";
        public static String TABLE_SMASTER4_13 = "CS06_SMASTER4_13";
        public static String TABLE_SMASTER4_14 = "CS06_SMASTER4_14";
        public static String TABLE_SMASTER4_15 = "CS06_SMASTER4_15";
        public static String TABLE_SMASTER4_16 = "CS06_SMASTER4_16";
        public static String TABLE_SMASTER4_17 = "CS06_SMASTER4_17";
        public static String TABLE_SMASTER4_18 = "CS06_SMASTER4_18";
		public static String TABLE_SMASTER4_19 = "CS06_SMASTER4_19";

		public static String TABLE_SMASTER2_09 = "CS06_SMASTER2_09";
        public static String TABLE_SMASTER2_10 = "CS06_SMASTER2_10";
        public static String TABLE_SMASTER2_11 = "CS06_SMASTER2_11";
        public static String TABLE_SMASTER2_12 = "CS06_SMASTER2_12";
        public static String TABLE_SMASTER2_13 = "CS06_SMASTER2_13";
        public static String TABLE_SMASTER2_14 = "CS06_SMASTER2_14";
        public static String TABLE_SMASTER2_15 = "CS06_SMASTER2_15";
        public static String TABLE_SMASTER2_16 = "CS06_SMASTER2_16";
        public static String TABLE_SMASTER2_17 = "CS06_SMASTER2_17";
        public static String TABLE_SMASTER2_18 = "CS06_SMASTER2_18";
		public static String TABLE_SMASTER2_19 = "CS06_SMASTER2_19";


		public static String TABLE_PORTFOLIO = "CS06_PORTFOLIO";

        /* Web 테스트 용*/
        //public static String TABLE_AVRG = "CS06_AVRG_nnew";
        //public static String TABLE_MASTER4 = "CS06_MASTER4_nnew";
        //public static string TABLE_SU_RANK = "CS06_SU_RANK_nnew";
        //public static string TABLE_LASTYEAR_RESULT = "CS06_LASTYEAR_RESULT_nnew";


        //public static String TABLE_목표대학_수시 = "CS06_SMASTER4_14";
        //public static String TABLE_목표대학_정시 = "CS06_MASTER4_MOI_14";

        public static String TABLE_목표대학_수시 = "CS06_SMASTER4_18";
        public static String TABLE_목표대학_정시 = "CS06_MASTER4_MOI_18";

        public static String TABLE_목표대학_수시_전문 = "CS06_SMASTER2_18";
        public static String TABLE_목표대학_정시_전문 = "CS06_MASTER2_18";

        public static String TABLE_목표대학_정시수능 = "CS06_MASTER4_18";

        #endregion

        #region 벤치마킹관련

        /// <summary>
        /// EndTime까지 모듈이 걸리는 시간을 시작한다
        /// </summary>
        /// <returns></returns>
        public static int StartTime()
        {
            return (DateTime.Now.Minute * 60000) + (DateTime.Now.Second * 1000) + DateTime.Now.Millisecond;
        }

        /// <summary>
        /// StartTime 으로부터 EndTime까지 걸리는 시간을 끝낸다
        /// </summary>
        /// <param name="sec"></param>
        /// <returns></returns>
        public static int EndTime( int sec )
        {

            int lastTime = (DateTime.Now.Minute * 60000) + (DateTime.Now.Second * 1000) + DateTime.Now.Millisecond - sec;

            return lastTime;
        }



        #endregion

        #region CP 및 WHERE 조건절 설정

        public static String WEB_GBN = "FP";
        public static String STU_USE_YEAR = "2017";
        //public static String TOP_ID = "";
        public static String STU_ID = "";
        public static String STU_NAME = "";
        public static String STU_GRADE = "";
        public static String HW_GBN = "";

        #endregion

        #region XML 경로 관련

        //public static string SERVER_PATCHES_XML_URI = "http://cspatch.futureplan.co.kr/CsPatchFile11/"; // 최신 패치정보 XML URI


        public static string SERVER_PATCHES_XML_URI = "http://cspatch.futureplan.co.kr/CsPatchFile11/"; // 최신 패치정보 XML URI
        public static string SERVER_PATCHES_XML_URI_2015 = "http://cspatch.futureplan.co.kr/CsPatchFile2015/"; // 최신 패치정보 XML URI
        //public static string SERVER_PATCHES_XML_URI_2017 = "http://cspatch.futureplan.co.kr/CsPatchFile2017/"; // 최신 패치정보 XML URI
        public static string SERVER_PATCHES_XML_URI_2018 = "http://cspatch.futureplan.co.kr/CsPatchFile2018/"; // 최신 패치정보 XML URI
																											   //public static string CLIENT_PATCHES_XML_URI = @"C:\Program Files\Cschool\IpsiPlanner\"; // 클라이언트 패치정보 XML 경로

		public static string SERVER_PATCHES_XML_URI_KCCE = "http://cdnpatch.futureplan.co.kr/Kcce2018/"; // 최신 패치정보 XML URI
		//Kcce 서버이관
		//public static string SERVER_PATCHES_XML_URI_KCCE = "https://jinrojinhak.procollege.kr/Kcce2018/"; // 최신 패치정보 XML URI
		#endregion

		#region C/S 경로 관련

		//  public static string CS_PATCHSERVER1_URI = "http://cspatch.futureplan.co.kr/CsPatchFile11/"; // 패치서버 1 URI
		//public static string CS_ROOT_PATH = @"C:\Program Files\Cschool\IpsiPlanner\"; // 클라이언트 설치 경로
		//public static string CS_IPPATHER_PATH = CS_ROOT_PATH + "IpPatcher.exe"; // 패치 프로그램 경로

		public static string CS_REPORT_DIR = @"c:\program files\ipsiplanner\";   // 배포시
        //public static string CS_REPORT_DIR = @"D:\CSchool\Source\CSCHOOL_PROJECT\MainApp.Report\";    // 박용호PC에서 개발시

        #endregion

        #region WEB 페이지 경로 관련

        /// <summary>
        /// 씨스쿨 홈
        /// </summary>
        public static string WEB_CSCHOOL_HOME = "http://www.futureplan.co.kr/";
        /// <summary>
        /// 티처스 웹페이지 홈
        /// </summary>
        public static string WEB_TEACHERS_HOME = "http://www.futureplan.co.kr/ippl_homepage/inc/CsCheck.asp"; // http://www.futureplan.co.kr/ippl_homepage/main.asp";
        /// <summary>
        /// 4년제 대학 로고 기본위치
        /// </summary>
        public static string WEB_UNIV4_LOGO = "http://cspatch.futureplan.co.kr/UnivLogo/Univ4/";
        /// <summary>
        /// 2년제 대학 로고 기본위치
        /// </summary>
        public static string WEB_UNIV2_LOGO = "http://www.futureplan.co.kr/web/images/logo/univ2/";
        /// <summary>
        /// 학원 로고 기본위치
        /// </summary>
        public static string WEB_HW_LOGO = "http://webservice.futureplan.co.kr/HwLogo/";
        /// <summary>
        /// 학생용 회원가입 URL
        /// </summary>
        public static string WEB_STU_JOIN = "http://www.futureplan.co.kr/cs_member/member_join.asp";
        /// <summary>
        /// 학생용 매뉴얼 다운로드 URL
        /// </summary>
        public static string WEB_STU_MANUAL = "http://www.futureplan.co.kr/ippl_homepage/data/manual_stu_pc_080707.pdf";
        /// <summary>
        /// 교사용 매뉴얼 다운로드 URL
        /// </summary>
        public static string WEB_TEACHER_MANUAL = "http://www.futureplan.co.kr/ippl_homepage/data/manual_tea_pc_080707.pdf";
        /// <summary>
        /// 학원용 매뉴얼 다운로드 URL
        /// </summary>
        public static string WEB_HW_MANUAL = "http://www.futureplan.co.kr/ippl_homepage/data/manual_tea_pc_080707.pdf";
        /// <summary>
        /// 수시 결제 페이지
        /// </summary>
        // public static string WEB_PAYMENT = "http://board.cschool.net/hap_report/08jungsi/johi_sg1.asp"; // 2008정시
        public static string WEB_PAYMENT = "http://www.futureplan.co.kr/hap_report/09susi2/susi2s3.asp";

        /// <summary>
        /// 2009 정시 결제 페이지
        /// </summary>
        public static string WEB_PAYMENT_JUNGSI = "http://www.futureplan.co.kr/hap_report/09jungsi/jungsi_s2.asp";
        /// <summary>
        /// 학생용 웹 열기 링크
        /// </summary>
        public static string WEB_IPL_OPEN = "http://www.futureplan.co.kr/web/teachers/stu_web_connect.aspx";
        /// <summary>
        /// 첫페이지 광고용 베너 이미지 경로
        /// </summary>
        public static string WEB_BANNER_PATH = "http://webservice.futureplan.co.kr/HwBanner/";

        #endregion

        #region C/S 창설정 관련

        // 최대 창 크기
        public static int MaxWidth = 0;
        public static int MaxHeight = 0;

        // 메임폼의 left, top 보정
        public static int MainLeftFix = 0;
        public static int MainTopFix = 0;

        public static bool IsMaximize = false;

        #endregion

        #region 입시플래너 매년 데이터 관리
        //public static string _전년도정시_testDate = "20131107_0";
        //public static string _올해정시_testDate = "20141113_0";
        //public static string _전년도정시_testDate = "20161117_0";
        //public static string _올해정시_testDate = "20171123_0";
        public static string _전년도정시_testDate = "20171123_0";
        public static string _올해정시_testDate = "20181115_0";

		public static string _SMaster4_올해 = TABLE_SMASTER4_17;
		public static string _SMaster4_전년 = TABLE_SMASTER4_16;

		public static string _Master4_올해 = TABLE_MASTER4_17;
		public static string _Master4_전년 = TABLE_MASTER4_16;

		public static string _Master2_올해 = TABLE_MASTER2_17;
		public static string _Master2_전년 = TABLE_MASTER2_16;

		public static string _SMaster2_올해 = TABLE_SMASTER2_17;
		public static string _SMaster2_전년 = TABLE_SMASTER2_16;

		public static string _Master4_moi_올해 = TABLE_MASTER4_MOI_17;
		public static string _Master4_moi_전년 = TABLE_MASTER4_MOI_16;


		public static string _Master2_올해_Kcce = TABLE_MASTER2_19;
		public static string _Master2_전년_Kcce = TABLE_MASTER2_18;

		public static string _SMaster2_올해_Kcce = TABLE_SMASTER2_19;
		public static string _SMaster2_전년_Kcce = TABLE_SMASTER2_18;

		#endregion

		public static string TONG_SUB_FILE_DOWNLOAD = "http://cspatch.futureplan.co.kr/CsSetupFile/"; //Tong 기타 파일 다운로드 경로

		public static string KCCE_SUB_FILE_DOWNLOAD = "http://cspatch.futureplan.co.kr/KcceSetupFile/";	//Kcce 기타 파일 다운로드 경로
	}
}
