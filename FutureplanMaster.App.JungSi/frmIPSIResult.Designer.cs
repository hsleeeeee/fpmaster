﻿namespace FutureplanMaster.App.JungSi
{
    partial class frmIPSIResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExe = new System.Windows.Forms.Button();
            this.chk_2016 = new System.Windows.Forms.CheckBox();
            this.chk_2017 = new System.Windows.Forms.CheckBox();
            this.chk_2018 = new System.Windows.Forms.CheckBox();
            this.txtUnivName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLinkCd = new System.Windows.Forms.TextBox();
            this.txtSUnivNm = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblUnivCd = new System.Windows.Forms.Label();
            this.txtWhereQuery = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.chk_2019 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnExe
            // 
            this.btnExe.Location = new System.Drawing.Point(346, 57);
            this.btnExe.Name = "btnExe";
            this.btnExe.Size = new System.Drawing.Size(87, 66);
            this.btnExe.TabIndex = 2;
            this.btnExe.Text = "시작";
            this.btnExe.UseVisualStyleBackColor = true;
            this.btnExe.Click += new System.EventHandler(this.btnExe_Click);
            // 
            // chk_2016
            // 
            this.chk_2016.AutoSize = true;
            this.chk_2016.Checked = true;
            this.chk_2016.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_2016.Location = new System.Drawing.Point(41, 62);
            this.chk_2016.Name = "chk_2016";
            this.chk_2016.Size = new System.Drawing.Size(48, 16);
            this.chk_2016.TabIndex = 3;
            this.chk_2016.Text = "2016";
            this.chk_2016.UseVisualStyleBackColor = true;
            // 
            // chk_2017
            // 
            this.chk_2017.AutoSize = true;
            this.chk_2017.Checked = true;
            this.chk_2017.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_2017.Location = new System.Drawing.Point(41, 84);
            this.chk_2017.Name = "chk_2017";
            this.chk_2017.Size = new System.Drawing.Size(48, 16);
            this.chk_2017.TabIndex = 4;
            this.chk_2017.Text = "2017";
            this.chk_2017.UseVisualStyleBackColor = true;
            // 
            // chk_2018
            // 
            this.chk_2018.AutoSize = true;
            this.chk_2018.Checked = true;
            this.chk_2018.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_2018.Location = new System.Drawing.Point(41, 106);
            this.chk_2018.Name = "chk_2018";
            this.chk_2018.Size = new System.Drawing.Size(48, 16);
            this.chk_2018.TabIndex = 5;
            this.chk_2018.Text = "2018";
            this.chk_2018.UseVisualStyleBackColor = true;
            // 
            // txtUnivName
            // 
            this.txtUnivName.Location = new System.Drawing.Point(108, 58);
            this.txtUnivName.Name = "txtUnivName";
            this.txtUnivName.Size = new System.Drawing.Size(227, 21);
            this.txtUnivName.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "입시년도";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(108, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "UNIVCD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(108, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "LINKCD";
            // 
            // txtLinkCd
            // 
            this.txtLinkCd.Location = new System.Drawing.Point(108, 101);
            this.txtLinkCd.Name = "txtLinkCd";
            this.txtLinkCd.Size = new System.Drawing.Size(227, 21);
            this.txtLinkCd.TabIndex = 9;
            // 
            // txtSUnivNm
            // 
            this.txtSUnivNm.Location = new System.Drawing.Point(108, 177);
            this.txtSUnivNm.Name = "txtSUnivNm";
            this.txtSUnivNm.Size = new System.Drawing.Size(227, 21);
            this.txtSUnivNm.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(346, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 66);
            this.button1.TabIndex = 12;
            this.button1.Text = "코드검색";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "대학명";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "UNIVCD";
            // 
            // lblUnivCd
            // 
            this.lblUnivCd.AutoSize = true;
            this.lblUnivCd.Location = new System.Drawing.Point(49, 284);
            this.lblUnivCd.Name = "lblUnivCd";
            this.lblUnivCd.Size = new System.Drawing.Size(0, 12);
            this.lblUnivCd.TabIndex = 15;
            // 
            // txtWhereQuery
            // 
            this.txtWhereQuery.Location = new System.Drawing.Point(108, 12);
            this.txtWhereQuery.Name = "txtWhereQuery";
            this.txtWhereQuery.Size = new System.Drawing.Size(322, 21);
            this.txtWhereQuery.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "조건쿼리";
            // 
            // btnTest
            // 
            //this.btnTest.Location = new System.Drawing.Point(39, 299);
            //this.btnTest.Name = "btnTest";
            //this.btnTest.Size = new System.Drawing.Size(87, 66);
            //this.btnTest.TabIndex = 18;
            //this.btnTest.Text = "테스트";
            //this.btnTest.UseVisualStyleBackColor = true;
            //this.btnTest.Click += new System.EventHandler(this.btnTest_Click_1);
            // 
            // chk_2019
            // 
            this.chk_2019.AutoSize = true;
            this.chk_2019.Checked = true;
            this.chk_2019.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_2019.Location = new System.Drawing.Point(39, 128);
            this.chk_2019.Name = "chk_2019";
            this.chk_2019.Size = new System.Drawing.Size(48, 16);
            this.chk_2019.TabIndex = 19;
            this.chk_2019.Text = "2019";
            this.chk_2019.UseVisualStyleBackColor = true;
            // 
            // frmIPSIResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 425);
            this.Controls.Add(this.chk_2019);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtWhereQuery);
            this.Controls.Add(this.lblUnivCd);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtSUnivNm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLinkCd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUnivName);
            this.Controls.Add(this.chk_2018);
            this.Controls.Add(this.chk_2017);
            this.Controls.Add(this.chk_2016);
            this.Controls.Add(this.btnExe);
            this.Name = "frmIPSIResult";
            this.Text = "frmIPSIResult";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExe;
        private System.Windows.Forms.CheckBox chk_2016;
        private System.Windows.Forms.CheckBox chk_2017;
        private System.Windows.Forms.CheckBox chk_2018;
        private System.Windows.Forms.TextBox txtUnivName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLinkCd;
        private System.Windows.Forms.TextBox txtSUnivNm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUnivCd;
        private System.Windows.Forms.TextBox txtWhereQuery;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox chk_2019;
    }
}