﻿using FPFrame.Api.Adminssions.SuSi;
using FPFrame.Database.Service.FPAdmissions;
using FPFrame.Entities.FPAdminssions;
using FPFrame.Test;

using MainApp.Common;
using MainApp.Common.Connection;
using MainApp.Common.Util;
using MainApp.Score;
using MainApp.UnivProcs19;
using MainApp.UnivProcs19.AutoCalNs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FutureplanMaster.App.JungSi
{
    public partial class MainForm : Form
    {
        public DefaultSettingValues config = new DefaultSettingValues
        {
            //CS06_MYJIWON_JUNGSI_IMIT_REAL = "futureplan_moi.dbo.CS06_MYJIWON_JUNGSI_IMIT_REAL_2017_back"
        };
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 기본 설정 셋팅
        /// </summary>
        private void initMain()
        {
            //서버에서 점수를 가져오기위해 설정
            Config.TABLE_SCORECP = "CS06_SCORE";
            Config.TABLE_SCORE = "CS06_SCORE";

            //최고 점수 연도 설정
            SetStdYear();
        }

        private async void Main()
        {
            if (comboBox1.SelectedIndex == -1)
                return;

            initMain();

            //내신 3학년 2학기 여부
            bool isNesin32 = cbNesin32.Checked;

            string univName = txtUnivName.Text;
            string majorName = txtMajorName.Text;
            string idx = txtIdx.Text;
            string jFinalCalcIdx = txtjIdx.Text;
            string stepOrder = string.IsNullOrEmpty(txtStepOrder.Text) ? "1" : txtStepOrder.Text;
            
            bool isDetail = chkIsDetail.Checked;

            List<string> unitCds = SetUnitCodeType();
            List<string> usrIds = SetStduentType();

            TestType testType = TestType.정시학생부지원가능점수;

            testType = SetTestType();

            bool isCollege23 = false;
            string masterName = config.CS06_MASTER4;

            if (rbCollege4.Checked == true)
            {
                masterName = config.CS06_MASTER4;
            }
            else if (rbCollege23.Checked == true)
            {
                isCollege23 = true;
                masterName = config.CS06_MASTER2;
            }

            switch (testType)
            {
                case TestType.정시지원가능점수:
                    {
                        //foreach (string unitCd in unitCds)
                        //{

                        string query = string.Format("SELECT  *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num AS tam_cnt"
                            + " FROM {4} "
                             //+ "where univ_code in ('039','071')"
                             + " where ('{0}' is null or '{0}'='' or univ_name='{0}' or univ_name like '%{0}%') "
                             //+ " where idx in ('2011','2012','2013','2014','2015','2016','2017','2018','2019')"
                             + " And ('{1}' is null or '{1}'='' or major_name='{1}' or major_name like '%{1}%') "
                             + " And ('{2}' is null or '{2}'='' or idx ='{2}')"
                             + " And sung_aper{3}>0"
                              //+ " And score_type = 4"
                              //+ " And score_type ='4'"
                              //+" where(univ_name like('을지대%') or univ_name like('가천%'))"
                              //+ " and stype_rem in ('일반전형2', '일반전형II')"

                              

                              //+ " where univ_code in ('015', '031')"
                              //+ " And sung_aper{3}>0"
                              + " order by idx asc", univName, majorName, idx, stepOrder, masterName);

                        DataTable mstDt = DefaultFns.QueryMaster(query, true, true);
                        GetMakeScoreResult(mstDt, Parser.ObjToInt(stepOrder), string.Format("{0}\\정시_배치점수_{1}_{2}단계.txt", config.Directory, DateTime.Now.ToString("yyyyMMdd"), stepOrder), isCollege23);
                        //}
                        break;

                    }                                       
                case TestType.정시학생점수확인:
                    {
                        string rstContents = string.Empty;
                        //foreach (string unitCd in unitCds)
                        //{
                            foreach (string usrId in usrIds)
                            {

                                string unitCd = "0";
                                string query = string.Format("SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num"
                                + " FROM {6} "
                                + " where ('{0}' is null or '{0}'='' or univ_name='{0}' or univ_name like '%{0}%') "
                                //+ " And ('{1}' is null or '{1}'='' or unit_code='{1}')"
                                + " And ('{2}' is null or '{2}'='' or major_name='{2}' or major_name like '%{2}%') "
                                + " And ('{3}' is null or '{3}'='' or idx ='{3}')"
                                + " And ('{4}' is null or '{4}'='' or jfinalCalcIdx ='{4}')"
                                + " And sung_aper{5}>0"
                                //+ "order by idx asc", univName, unitCd, majorName, idx, jFinalCalcIdx, stepOrder, masterName);
                                + "order by idx asc", univName, unitCd, majorName, idx, jFinalCalcIdx, stepOrder, masterName);

                                string topId = null;
                                DataTable mstDt = DefaultFns.QueryMaster(query, true);
                                //string rst = JunSiStuCalc(usrId, topId, mstDt, isDetail, isCollege23, string.Format("정시산출_{0}_{1}.txt", unitCd, isDetail, usrId));
                                string rst = JunSiStuCalc(usrId, topId, mstDt, isDetail, isCollege23, config.TestDate, isNesin32, stepOrder, string.Format("정시산출_{0}_{1}.txt", unitCd, isDetail, usrId));

                                rstContents += rst + "------------------------------------------\r\n";
                            }
                        //}
                        this.txtResult.Text = rstContents;
                        break;
                    }
                case TestType.전문대_정시지원가능점수:
                    {
                        //foreach (string unitCd in unitCds)
                        //{
                        masterName = "DBO.CS06_MASTER2_19";
                        string query = string.Format("SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num AS tam_cnt"
                            + " FROM {4} "
                            + " where ('{0}' is null or '{0}'='' or univ_name='{0}' or univ_name like '%{0}%') "
                            + " And ('{1}' is null or '{1}'='' or unit_code='{1}')"
                            //+ " And ('{2}' is null or '{1}'='' or major_name='{1}' or major_name like '%{1}%') "
                            + " And ('{2}' is null or '{2}'='' or idx ='{2}')"
                            + " And sung_aper{3}>0"
                            //+ "order by idx asc", univName, majorName, idx, stepOrder, masterName);
                            //+ "where(univ_name like('을지대%') or univ_name like('가천%'))"
                            //+ "and stype_rem in ('일반전형2', '일반전형II')"

                            + " order by idx asc", univName, majorName, idx, stepOrder, masterName);

                        DataTable mstDt = DefaultFns.QueryMaster(query, true, true);
                        GetMakeScoreResult(mstDt, Parser.ObjToInt(stepOrder), string.Format("{0}\\정시_배치점수_{1}_{2}단계.txt", config.Directory, DateTime.Now.ToString("yyyyMMdd"), stepOrder), true);
                        //}
                        break;
                    }
            }
        }

        private TestType SetTestType()
        {
            TestType testType = TestType.정시지원가능점수;

            string type_name = comboBox1.SelectedItem.ToString();
            switch (type_name)
            {
                case "정시지원가능점수":
                    testType = TestType.정시지원가능점수;
                    break;
                case "전문대_정시지원가능점수":
                    testType = TestType.전문대_정시지원가능점수;
                    break;
                case "수시지원가능점수":
                    testType = TestType.수시지원가능점수;
                    break;
                case "정시학생점수확인":
                    testType = TestType.정시학생점수확인;
                    break;
                case "수시학생점수확인":
                    testType = TestType.수시학생점수확인;
                    break;
                case "정시학생부지원가능점수":
                    testType = TestType.정시학생부지원가능점수;
                    break;
                case "입결정시산출점수":
                    testType = TestType.입결정시산출점수;
                    break;
                case "정시내신점수확인":
                    testType = TestType.정시내신점수확인;
                    break;

            }

            return testType;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.Clear();
            Main();
        }

        /// <summary>
        ///  조회하고자 하는 계열확인
        /// </summary>
        /// <returns></returns>
        private List<string> SetUnitCodeType()
        {
            List<string> unitCds = new List<string>();

            if (cbUnitCode1.Checked == true)
                unitCds.Add("1");
            if (cbUnitCode2.Checked == true)
                unitCds.Add("2");
            if (cbUnitCode3.Checked == true)
                unitCds.Add("3");

            return unitCds;
        }

        /// <summary>
        /// 확인할 성적표 확인?
        /// </summary>
        /// <returns></returns>
        private List<string> SetStduentType()
        {
            List<string> usrIds = new List<string>();

            if (cb_StudentU1.Checked)
                usrIds.Add(config.UsrId_UnitCode1);
            if (cb_StudentU2.Checked)
                usrIds.Add(config.UsrId_UnitCode2);

            return usrIds;
        }

        /// <summary>
        /// 확인할 최고점수 연도 설정
        /// </summary>
        /// <returns></returns>
        private void SetStdYear()
        {
            switch (comboBoxStdYear.Text)
            {
                
                case "2018":
                    config.TestDate = "20181115_0";
                    config.StuUseYear = "2018";
                    break;
                case "2019":
                    config.TestDate = "20191114_0";
                    config.StuUseYear = "2019";
                    break;
            }
        }

        #region 산출계산용함수 

        /// <summary>
        /// 내신산출 지원등급 조회
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="query"></param>
        /// <param name="step"></param>
        /// <param name="savePath"></param>
        /// <param name="isCollege"></param>
        /// <param name="is3GradeResult"></param>
        private void MakeApplyScore(string stuId, string query, string step, string savePath, bool isCollege, bool isJungSi, bool isNesin32)
        {
            // 기존 산출로직 DB 커넥션
            WebConnection webConnection = new WebConnection(true);
            UnivSn_J univSn_J = new UnivSn_J(webConnection);

            //성적조회
            List<NSScore> lstScore = CalcNs.GetNesinScore(stuId, true, isNesin32);

            //마스터테이블조회
            DataTable dt = QueryMasterHak(univSn_J, query);
            string resultContens = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                List<NSScore> nsScores = new List<NSScore>();
                foreach (NSScore nsScore in lstScore)
                {
                    nsScores.Add(nsScore);
                }

                string result = string.Empty;
                result = GetMakeApplyScoreResult(stuId, step, isCollege, dr, nsScores, univSn_J, isJungSi);
                resultContens += result;

                Console.WriteLine(result);
            }
            System.IO.File.WriteAllText(savePath, resultContens, Encoding.Default);
        }

        /// <summary>
        /// 내신산출 지원등급 결과 저장
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="step"></param>
        /// <param name="isCollege"></param>
        /// <param name="drUnivCalcInfo"></param>
        /// <param name="nsScoreList"></param>
        /// <param name="univSn_J"></param>
        /// <returns></returns>
        public string GetMakeApplyScoreResult(string stuId, string step, bool isCollege, DataRow drUnivCalcInfo, List<NSScore> nsScoreList, UnivSn_J univSn_J, bool isJungSi)
        {
            //CS06_SMASTER4 univCalcInfo = new CS06_SMASTER4(drUnivCalcInfo, isCollege);

            //string info = string.Empty;
            //try
            //{
            //    ApplyScoreResult result = CalcNsFunc.MakeApplyScore(stuId, step, isCollege, drUnivCalcInfo, nsScoreList, univSn_J, isJungSi);
            //    info = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\r\n",
            //                univCalcInfo.idx, univCalcInfo.univ_name,
            //                univCalcInfo.major_name, univCalcInfo.hak_aper1, univCalcInfo.hak_aper2,
            //                result.ApplyGrade,
            //                result.PrevGrade, result.PrevScore,
            //                result.NextGrade, result.NextScore,
            //                result.FinalScore);
            //}
            //catch (Exception ex)
            //{
            //    info = string.Format("{0}\t\t{1}\t{2}\t{3}\t{4}\t{5}\r\n",
            //        univCalcInfo.idx,
            //        univCalcInfo.univ_name,
            //        univCalcInfo.stype_rem,
            //        univCalcInfo.major_name,
            //        "",
            //        ex.Message.ToString()
            //        );
            //}

            //return info;

            return string.Empty;
        }

        /// <summary>
        /// 정시산출 지원가능점수
        /// </summary>
        /// <param name="mstDt"></param>
        /// <returns></returns>
        private List<UnivScoreResult> GetMakeScoreResult(DataTable mstDt, int stepOrder, string fileName, bool isCollege23)
        {

            DataTable GradeDt = Proc.CreateCommandText(string.Format("select * from {0}", config.CS06_GRADE_TABLE_JUNGSI));
            List<UnivScoreResult> rstScore = new List<UnivScoreResult>();
            string resultContens = string.Empty;
            foreach (DataRow mstDr in mstDt.Rows)
            {
                try
                {
                    CollegeScholasticAbilityTest csat = new CollegeScholasticAbilityTest(mstDr);
                    UnivScoreResult rst = null;
                    rst = csat.UnivluateCAST(GradeDt, stepOrder, isCollege23);
                    double minuse = 0;
                    //2019.12.20 수학가산점 0.5
                    if (rst.MasterInfo.univ_name.Equals("서강대"))
                    {
                        if (rst.AddScores.MathAdd > 0)
                        {
                            minuse = rst.AddScores.MathAdd * 0.5f;
                        }
                    }
                    string context = "IDX:" + rst.MasterInfo.idx.ToString() + "\r\n"
                + "대학명:" + rst.MasterInfo.univ_name + "\r\n"
                + "학과명:" + rst.MasterInfo.major_name + "\r\n"
                + "지역코드:" + rst.MasterInfo.univ_area + "\r\n"
                + "전형명:" + rst.MasterInfo.stype_rem + "\r\n"
                + "PARTNAME:" + rst.MasterInfo.part_name + "\r\n"
                + "점수타입:" + rst.MasterInfo.score_type + "\r\n"
                + "기본산출점수 :" + rst.upScore.ToString() + "\r\n"
                + "국어점수 :" + rst.kor.ToString() + "\r\n"
                + "수학점수 :" + rst.math.ToString() + "\r\n"
                + "탐구점수 :" + rst.tam.ToString() + "\r\n"
                + "영어점수 :" + rst.eng.ToString() + "\r\n"
                + "한국사점수 :" + rst.korHis.ToString() + "\r\n"
                + "제2외국어점수 :" + rst.lang2.ToString() + "\r\n"
                + "국어가산점수 :" + rst.AddScores.KorAdd.ToString() + "\r\n"
                + "수학가산점수 :" + rst.AddScores.MathAdd.ToString() + "\r\n"
                + "탐구가산점수 :" + rst.AddScores.TamAdd.ToString() + "\r\n"
                + "영어가산점수 :" + rst.AddScores.EngAdd.ToString() + "\r\n"
                + "한국사가산점수 :" + rst.AddScores.KorHisAdd.ToString() + "\r\n"
                + "제2외국어가산점수 :" + rst.AddScores.Lang2Add.ToString() + "\r\n"

                + "국어반영 :" + rst.korAper.ToString() + "\r\n"
                + "수반영비율 :" + rst.mathAper.ToString() + "\r\n"
                + "탐반영비율 :" + rst.tamAper.ToString() + "\r\n"
                + "영반영비율 :" + rst.engAper.ToString() + "\r\n"
                + "총산출점수:" + (Convert.ToDouble(rst.MySumScore)- minuse).ToString();

                    //Console.WriteLine(string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), rst.MySumScore));
                    Console.WriteLine(context);
                    resultContens += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\r\n"
                        , mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), (Convert.ToDouble(rst.MySumScore) - minuse).ToString(), rst.korAper, rst.mathAper, rst.tamAper, rst.engAper, rst.tamAper);

                    //resultContens += string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\r\n"
                    //    , mstDr["idx"].ToString()
                    //    , mstDr["univ_name"].ToString()
                    //    , mstDr["part_code"].ToString()
                    //    , mstDr["part_name"].ToString()
                    //    , mstDr["tam_subject_num"].ToString()
                    //    , mstDr["score_type"].ToString()
                    //    , rst.MySumScore
                    //    , rst.kor
                    //    , rst.math
                    //    , rst.tam
                    //    , rst.eng
                    //    , rst.korHis
                    //    , rst.lang2
                    //    , rst.AddScores.KorAdd + rst.AddScores.MathAdd + rst.AddScores.TamAdd
                    //    + rst.AddScores.EngAdd + rst.AddScores.KorHisAdd + rst.AddScores.Lang2Add 
                    //    , csat.Parameter.kor
                    //    ,csat.Parameter.math
                    //    ,csat.Parameter.engGra
                    //    ,csat.Parameter.tam
                    //    , csat.Parameter.tam1
                    //    ,csat.Parameter.tam2);

                }
                catch (Exception ex)
                {
                    //string context = "IDX:" + mstDr["idx"].ToString() + "\r\n"
                    //            + "대학명:" + mstDr["univ_name"].ToString() + "\r\n"
                    //            + "소스위치:" + ex.StackTrace.ToString() + "\r\n"
                    //            + "에러내용:" + ex.Message.ToString();
                    //Console.WriteLine(context);
                    //resultContens += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), ex.Message);
                }
            }
            System.IO.File.WriteAllText(fileName, resultContens, Encoding.Default);
            return rstScore;
        }
       
        /// <summary>
        /// 정시 학생용 테스트용
        /// </summary>
        /// <param name="stuid"></param>
        /// <param name="topid"></param>
        /// <param name="mstDt"></param>
        /// <returns></returns>
        private string JunSiStuCalc(string stuid, string topid, DataTable mstDt, bool isViewDetail, bool isCollege23, string testDate, bool isNesin32, string step_order, string fileName = "")
        {
            DataTable rstDt = new DataTable();

            //수능성적조회
            SnScore sn = CollegeScholasticAbilityTest.SetSnScore(stuid, testDate);
            //내신
            List<NSScore> ns = CalcNs.GetNesinScore(stuid, true, isNesin32);

            //기존산출로직내신객체
            NsScore ftNsScore = CalcNs.GetFtNsScore();
            NsNonScore nsNonScore = CalcNs.GetFtNsNonFromSql(stuid, config.StuUseYear);
            UserInfo usrInfo = new UserInfo
            {
                StuId = stuid,
                ScoHak = "3",
                StuUseYear = "2019",
                TestDate = testDate,
                TopId = null,
                StuGrade = "2019"
            };


            string name = config.UsrId_UnitCode1.Equals(stuid) ? "인문" : "자연";

            string trueContents = string.Empty;
            string falseContents = string.Empty;
            trueContents += string.Format("{0}\t{1}\t{2}\t"
                + "{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t"
                + "{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\r\n"
                , "IDX", "UnivName", "MySumScore"
                , "MyUpScore", "KorMyUpScore", "MathMyUpScore", "TamMyUpScore", "EngMyUpScore", "KorHisMyUpScore", "Lang2MyUpScore"
                , "MyAdd", "MyKorAdd", "MyKorAdd", "MyTamAdd", "MyEngAdd", "MyKorHisAdd", "MyLang2Add");

             falseContents += string.Format("{0}\t{1}\t{2}\t"
                + "{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t"
                + "{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\r\n"
                , "IDX", "UnivName", "MySumScore"
                , "MyUpScore", "KorMyUpScore", "MathMyUpScore", "TamMyUpScore", "EngMyUpScore", "KorHisMyUpScore", "Lang2MyUpScore"
                , "MyAdd", "MyKorAdd", "MyKorAdd", "MyTamAdd", "MyEngAdd", "MyKorHisAdd", "MyLang2Add");

            string printContents = string.Empty;
            foreach (DataRow mstDr in mstDt.Rows)
            {
                NSCalc master = new NSCalc
                {
                    StypeRem = mstDr["stype_rem"].ToString(),
                    UnivCode = mstDr["univ_code"].ToString(),
                    MajorCode = mstDr["major_code"].ToString(),
                    RtimeCode = mstDr["rtime_code"].ToString(),
                    StypeCode = mstDr["stype_code"].ToString(),
                };

                try
                {
                    DataRow resultDr = CollegeScholasticAbilityTest.CalculateCSAT(usrInfo, master, sn, ftNsScore, nsNonScore, isCollege23);
                    rstDt.ImportRow(resultDr);
                    UnivScoreResult usr = new UnivScoreResult(resultDr, step_order);
                    string context = string.Empty;
                    if (isViewDetail)
                    {
                        context = name + " : "
                        + "IDX:" + mstDr["idx"].ToString()
                        + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                        + "\r\n" + "학과명:" + mstDr["major_name"].ToString()
                        + "\r\n" + "전형명:" + mstDr["stype_rem"].ToString()
                        + "\r\n" + "진짜산출점수~:" + (Parser.ObjToFloat(usr.MyUpScore) + Parser.ObjToFloatA(usr.MyAdd)).ToString()
                        + "\r\n" + "총산출점수:" + usr.MySumScore
                        + "\r\n" + "산출점수:" + usr.MyUpScore
                        + "\r\n" + "국어산출점수:" + usr.KorMyUpScore
                        + "\r\n" + "수학산출점수:" + usr.MathMyUpScore
                        + "\r\n" + "탐구산출점수:" + usr.TamMyUpScore
                        + "\r\n" + "영어산출점수:" + usr.EngMyUpScore
                        + "\r\n" + "한국사산출점수:" + usr.KorHisMyUpScore
                        + "\r\n" + "제2외국어산출점수:" + usr.Lang2MyUpScore
                        + "\r\n" + "가산점:" + usr.MyAdd
                        + "\r\n" + "국어가산점:" + usr.MyKorAdd
                        + "\r\n" + "수학가산점:" + usr.MyMathAdd
                        + "\r\n" + "탐구가산점:" + usr.MyTamAdd
                        + "\r\n" + "영어가산점:" + usr.MyEngAdd
                        + "\r\n" + "한국사가산점:" + usr.MyKorHisAdd
                        + "\r\n" + "제2외국어가산점:" + usr.MyLang2Add
                        + "\r\n" + "-------------------------------------------------------------"
                        + "\r\n";
                    }
                    else
                    {
                        context = name + " : "
                                        + "IDX:" + mstDr["idx"].ToString()
                                        + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                                        + "\r\n" + "학과명:" + mstDr["major_name"].ToString()
                                        + "\r\n" + "전형명:" + mstDr["stype_rem"].ToString()
                                        + "\r\n" + "총산출점수:" + usr.MySumScore
                                        + "\r\n" + "-------------------------------------------------------------"
                                        + "\r\n";
                    }

                    Console.WriteLine(context);
                    trueContents += string.Format("{0}\t{1}\t{2}\t"
                                    + "{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t"
                                    + "{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\r\n"
                                    , mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), (Parser.ObjToFloat(usr.MyUpScore) + Parser.ObjToFloatA(usr.MyAdd)).ToString()
                                    , usr.MyUpScore, usr.KorMyUpScore, usr.MathMyUpScore, usr.TamMyUpScore, usr.EngMyUpScore, usr.KorHisMyUpScore, usr.Lang2MyUpScore
                                    , usr.MyAdd, usr.MyKorAdd, usr.MyKorAdd, usr.MyTamAdd, usr.MyEngAdd, usr.MyKorHisAdd, usr.MyLang2Add);
                    printContents += context;
                }
                catch (Exception EX)
                {
                    string context = "IDX:" + mstDr["idx"].ToString() + "\r\n"
                             + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                             + "\r\n" + "에러내용:" + EX.StackTrace;
                    Console.WriteLine(context);
                    falseContents += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString()
                        , mstDr["univ_name"].ToString()
                        , EX.StackTrace);
                }
            }

            fileName = string.Format(@"{0}\{1}_T.txt", config.Directory, "결과_"+ DateTime.Now.ToShortDateString());
            System.IO.File.WriteAllText(fileName, trueContents, Encoding.Default);

            fileName = "";
            fileName = string.Format(@"{0}\{1}_F.txt", config.Directory, "결과_" + DateTime.Now.ToShortDateString());
            System.IO.File.WriteAllText(fileName, falseContents, Encoding.Default);

            return printContents;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="query"></param>
        /// <param name="step"></param>
        /// <param name="savePath"></param>
        /// <param name="isCollege"></param>
        /// <param name="isJungSi"></param>
        /// <param name="stuUseYear"></param>
        private string VerifyNsCalc(string stuId, string query, string step, string savePath, bool isCollege, bool isJungSi, string stuUseYear, bool isNesin32)
        {
            //출력용
            string textBoxtext = "";

            //기존 산출로직 DB 커넥션
            WebConnection webConnection = new WebConnection(true);
            UnivSn_J univSn_J = new UnivSn_J(webConnection);

            //성적조회
            List<NSScore> lstScore = CalcNs.GetNesinScore(stuId, true, isNesin32);

            //마스터테이블 조회
            DataTable dt = QueryMasterHak(univSn_J, query);

            //비교내신성적 조회
            NsNonScore nsNonScore = CalcNs.GetFtNsNonFromSql(stuId, stuUseYear);
            string resultContens = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                List<NSScore> nsScores = new List<NSScore>();
                foreach (NSScore nsScore in lstScore)
                {
                    nsScores.Add(nsScore);
                }

                string result = string.Empty;
                result = CalcNsFunc.CheckNsCalc(stuId, step, isCollege, dr, nsScores, nsNonScore, univSn_J, isJungSi);
                resultContens += result;

                Console.WriteLine(result);

                var resultSplits = result.Split('\t');
                textBoxtext +=
                    "IDX : " + resultSplits[0] + "\r\n" +
                    "학교명 : " + resultSplits[1] + "\r\n" +
                    "학과명 : " + resultSplits[2] + "\r\n" +
                    "1차반영비율 : " + resultSplits[6] + "\r\n" +
                    "2차반영비율 : " + resultSplits[7] + "\r\n" +
                    "1학년비율 : " + resultSplits[3] + "\r\n" +
                    "2학년비율 : " + resultSplits[4] + "\r\n" +
                    "3학년비율 : " + resultSplits[5] + "\r\n" +
                    "1학년평균 : " + resultSplits[8] + "\r\n" +
                    "2학년평균 : " + resultSplits[9] + "\r\n" +
                    "3학년평균 : " + resultSplits[10] + "\r\n" +
                    "총 평균 : " + resultSplits[11] + "\r\n" +
                    "지원가능등급 : " + resultSplits[14] + "\r\n" +
                    "최종점수 : " + resultSplits[15] + "\r\n" +
                    "총점만점 : " + resultSplits[16] + "\r\n" +
                    "---------------------------------------\r\n";


            }

            System.IO.File.WriteAllText(savePath, resultContens, Encoding.Default);
            return textBoxtext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="univSn_J"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private DataTable QueryMasterHak(UnivSn_J univSn_J, string query)
        {
            DataTable dt = Proc.CreateCommandText(query);
            univSn_J.AddColumn(false, false, dt);
            return dt;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBox1.SelectedItem.ToString());

            if (comboBox1.SelectedItem.ToString().Equals("정시지원가능점수"))
            {
                cbUnitCode1.Checked = true;
                cbUnitCode2.Checked = true;
                cbUnitCode3.Checked = true;
                cbUnitCode1.Enabled = false;
                cbUnitCode2.Enabled = false;
                cbUnitCode3.Enabled = false;
            }
            else
            {
                cbUnitCode1.Enabled = true;
                cbUnitCode2.Enabled = true;
                cbUnitCode3.Enabled = true;
            }
        }

        /// <summary>
        /// 내신배치점수
        /// </summary>
        /// <param name="enterYear"></param>
        /// <param name="isCollege"></param>
        /// <param name="isJungsi"></param>
        private void GetApplyScore(string enterYear, bool isCollege, bool isJungsi)
        {
            //DataTable dt_1 = new DataTable();
            //dt_1.Columns.Add("idx");
            //dt_1.Columns.Add("score");

            //DataTable dt_2 = new DataTable();
            //dt_2.Columns.Add("idx");
            //dt_2.Columns.Add("score");

            //string result = string.Empty;
            //string Directory = @"D:\배치점수";
            //SubjectService _subjectService = new SubjectService();
            //NesinCalcConfigService _nesinCalcConfigService = new NesinCalcConfigService();
            //List<Master> lstmaster = _nesinCalcConfigService.GetMaster(null, null, enterYear, isCollege,isJungsi);
            //List<NesinCalcConfig> nesinCalcConfig = _nesinCalcConfigService
            //    .GetNesinCalcConfig(enterYear).ToList();
            //List<SubjectFilterInfo> subjectFileInfo = _subjectService
            //    .GetSubjectInfo().ToList();

            //string resultContent_1 = string.Empty;
            //string resultContent_2 = string.Empty;
            ////foreach(Master m in lstmaster.Where(x=>x.IDx=="11669").ToList())
            //foreach (Master m in lstmaster)
            //{
            //    try
            //    {
            //        NesinCalcConfig n = nesinCalcConfig.Where(x => x.FinalCalcIDx == m.FinalCalcIDx).SingleOrDefault();
            //        SubjectFilterInfo sOri = subjectFileInfo.Where(x => x.SubjectIDx.ToString() == n.SubjectIDx).SingleOrDefault();
            //        SubjectFilterInfo sCopy = sOri.Clone() as SubjectFilterInfo;


            //        //SubjectFilterInfo s = subjectFileInfo.Where(x => x.SubjectIDx.ToString() == n.SubjectIDx).SingleOrDefault();
            //        CalcNesinScore c = CalcNesinUtil.GetScore(Convert.ToDouble(m.ApplyHakGrade), sCopy, n);


            //        if (FPFrame.Common.Core.FPParser.ToDouble(m.HakAper1) > 0)
            //        {
            //            DataRow dr = dt_1.NewRow();
            //            dr["idx"] = m.IDx;
            //            dr["score"] = c.FinalScore_1;
            //            string info_1 = string.Format("{0}\t{1}\r\n", m.IDx, c.FinalScore_1);
            //            Console.WriteLine(info_1);
            //            dt_1.Rows.Add(dr);
            //        }
            //        if (FPFrame.Common.Core.FPParser.ToDouble(m.HakAper2) > 0)
            //        {
            //            DataRow dr = dt_2.NewRow();
            //            dr["idx"] = m.IDx;
            //            dr["score"] = c.FinalScore_2;
            //            string info_2 = string.Format("{0}\t{1}\r\n", m.IDx, c.FinalScore_2);

            //            Console.WriteLine(info_2);
            //            dt_2.Rows.Add(dr);
            //        }
            //    }

            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(m.IDx + ":" + ex.Message);
            //        if (FPFrame.Common.Core.FPParser.ToDouble(m.HakAper1) > 0)
            //        {
            //            DataRow dr = dt_1.NewRow();
            //            dr["idx"] = m.IDx;
            //            dr["score"] = ex.Message;
            //            string info_1 = string.Format("{0}\t{1}\r\n", m.IDx, ex.Message);
            //            Console.WriteLine(info_1);
            //            dt_1.Rows.Add(dr);
            //        }
            //        if (FPFrame.Common.Core.FPParser.ToDouble(m.HakAper2) > 0)
            //        {
            //            DataRow dr = dt_2.NewRow();
            //            dr["idx"] = m.IDx;
            //            dr["score"] = ex.Message;
            //            string info_2 = string.Format("{0}\t{1}\r\n", m.IDx, ex.Message);

            //            Console.WriteLine(info_2);
            //            dt_2.Rows.Add(dr);
            //        }
            //    }

            //}

            //string savePath_1 = string.Format(@"{0}\\{1}_내신배치점수_1.xlsx", Directory, DateTime.Now.ToString("yyyyMMdd"));
            //FPFrame.Common.Core.FpExcel.ExportDataSet(dt_1, string.Format(@"{0}\\{1}_내신배치점수_1.xlsx", Directory, DateTime.Now.ToString("yyyyMMdd")));


            //string savePath_2 = string.Format(@"{0}\\{1}_내신배치점수_2.xlsx", Directory, DateTime.Now.ToString("yyyyMMdd"));
            //FPFrame.Common.Core.FpExcel.ExportDataSet(dt_2, string.Format(@"{0}\\{1}_내신배치점수_2.xlsx", Directory, DateTime.Now.ToString("yyyyMMdd")));
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }




        #endregion

        /// <summary>
        /// 내신성적조회
        /// </summary>
        /// <returns></returns>
    }

    public class DefaultFns
    {
        public static string MakeRstFile(string directory, string fileName, string resultContens)
        {
            fileName = string.Format(@"{0}\{1}.txt", directory, fileName);
            System.IO.File.WriteAllText(fileName, resultContens, Encoding.Default);

            return fileName;
        }

        public static DataTable QueryMaster(string query, bool isJugSi, bool isMakeApplyScore = false)
        {
            DataTable dt = Proc.CreateCommandText(query);

            if (!isMakeApplyScore)
            {
                WebConnection webConnection = new WebConnection(true);
                UnivSn_J univSn_J = new UnivSn_J(webConnection);
                univSn_J.AddColumn(false, isJugSi, dt);
            }
            else
            {
                dt.Columns.Add("math_aper", typeof(float));
                dt.Columns.Add("tam_aper", typeof(float));
                dt.Columns.Add("kor_my_up_score1", typeof(float));
                dt.Columns.Add("math_my_up_score1", typeof(float));
                dt.Columns.Add("eng_my_up_score1", typeof(float));
                dt.Columns.Add("tam_my_up_score1", typeof(float));
                dt.Columns.Add("lang2_my_up_score1", typeof(float));

                dt.Columns.Add("kor_aper", typeof(float));
                dt.Columns.Add("eng_aper", typeof(float));

                dt.Columns.Add("engPerManjum", typeof(bool));
                dt.Columns.Add("engGraAddScore", typeof(bool));
                dt.Columns.Add("korHisGraAddScore", typeof(bool));
            }

            return dt;
        }

        public static string SetMasterQueryByIdx(string tbMaster, string idx)
        {
            string query = string.Format("SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num"
                                + " FROM {0} "
                                + " where idx='{1}'", tbMaster, idx);

            return query;

        }

        public static List<Master4Info> GetPartDivNew()
        {
            DataTable dt = Proc.CreateCommandText("select part_method_code,part_name from CS06_PART_DIV_NEW");
            List<Master4Info> lstPartDivNew = new List<Master4Info>();
            foreach (DataRow dr in dt.Rows)
            {
                Master4Info part = new Master4Info
                {
                    Part_method_code = dr["part_method_code"].ToString(),
                    Part_name = dr["part_name"].ToString()
                };
                lstPartDivNew.Add(part);
            }
            return lstPartDivNew;
        }
    
    }
}
