﻿
using FPFrame.Test;
using MainApp.Common.Connection;
using MainApp.Common.Util;
using MainApp.Score;
using MainApp.UnivProcs19;
using MainApp.UnivProcs19.AutoCalNs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// 배치점수 만들기
/// </summary>
namespace FutureplanMaster.App.JungSi
{
  
    
    class Program
    {
        public static string Directory = @"D:\정시산출";
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//Application.Run(new MainForm());
			//Application.Run(new frmIPSIResult());
			Application.Run(new SelectForm());

            /*
            TestType testType = TestType.정시학생점수확인;

            switch (testType)
            {
                case TestType.정시학생부지원가능점수:
                    {
                        break;
                    }
                case TestType.수시지원가능점수:
                    {
                        string stuId = "DZK00044";
                        string query = "SELECT *"
                                    + " FROM dbo.cs06_smaster4_17 "
                                    + " where hak_aper1>0"
                                    + " order by idx asc";
                        string step="1";
                        string savePath= string.Format(@"D:\정시산출\{0}_내신배치점수.txt", DateTime.Now.ToString("yyyyMMdd")); 
                        bool isCollege = false;
                        MakeApplyScore(stuId, query, step, savePath, isCollege);
                        break;
                    }
                case TestType.정시지원가능점수:
                    {
                        string query = "SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num AS tam_cnt"
                                    + " FROM dbo.cs06_master4_17 "
                                    + " where unit_code='1'"
                                    + " order by idx asc";
                        int stepOrder = 1;
                        DataTable mstDt = QueryMasterSung(query,true);
                        GetMakeScoreResult(mstDt,stepOrder);
                        break;

                    }
                case TestType.수시학생점수확인:
                    {
                        string usrId = "DZK00044";
                        string query = "SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num AS tam_cnt"
                                    + " FROM dbo.cs06_master4_17 "
                                    + " where hak_aper1>0"
                                    + " order by idx desc";
                        string step = "1";
                        string savepath = @"D:\내신산출\TXT\20181029_1_정시내신.txt";
                        bool isCollege = false;
                        bool isJungSi = true;
                        VerifyNsCalc(usrId, query, step, savepath, isCollege, isJungSi);
                        break;
                    }
                //검증용
                case TestType.정시학생점수확인:
                    {
                        string[] unitCds = { "1" };
                        //string[] usrIds = { "DZK00019", "DZK00044"};
                        string[] usrIds = { "DZK00019" };
                        foreach (string unitCd in unitCds)
                        {
                            foreach (string usrId in usrIds)
                            {
                                string query = string.Format("SELECT *, total_score1 * sung_aper1 / 100 AS SUNG_TOT1, total_score2 * sung_aper2 / 100 AS SUNG_TOT2, tam_subject_num"
                                            + " FROM dbo.cs06_master4_17 "
                                            //+ " where unit_code='{0}'"
                                            + "where idx='1'"
                                            + " order by idx asc", unitCd);

                                string topId = null;
                                DataTable mstDt = QueryMasterSung(query);
                                DataTable rstDt = JunSiStuCalc(usrId, topId, mstDt, string.Format("정시산출_{0}_{1}.txt",unitCd,usrId));
                            }
                        }
                        break;
                    }
            }  
			*/
        }
        

        /// <summary>
        /// 내신산출용
        /// </summary>
        /// <param name="univSn_J"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public static DataTable QueryMasterHak(UnivSn_J univSn_J, string query)
        {           
            DataTable dt = Proc.CreateCommandText(query);
            univSn_J.AddColumn(false, false, dt);
            return dt;
        }

        /// <summary>
        /// 수능산출용
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static DataTable QueryMasterSung(string query, bool isMakeApplyScore=false)
        {
            DataTable dt = Proc.CreateCommandText(query);

            if (!isMakeApplyScore)
            {
                WebConnection webConnection = new WebConnection(true);
                UnivSn_J univSn_J = new UnivSn_J(webConnection);
                univSn_J.AddColumn(false, true, dt);
            }
            else
            {
                dt.Columns.Add("math_aper", typeof(float));                     // 수리영역 반영비율
                dt.Columns.Add("tam_aper", typeof(float));                      // 탐구영역 반영비율
                dt.Columns.Add("kor_my_up_score1", typeof(float));              // 탐구영역 반영비율
                dt.Columns.Add("math_my_up_score1", typeof(float));             // 탐구영역 반영비율
                dt.Columns.Add("eng_my_up_score1", typeof(float));              // 탐구영역 반영비율
                dt.Columns.Add("tam_my_up_score1", typeof(float));              // 탐구영역 반영비율
                dt.Columns.Add("lang2_my_up_score1", typeof(float));            // 탐구영역 반영비율
                //dt.Columns.Add("tam_cnt", typeof(float));              // 탐구영역 반영비율
                dt.Columns.Add("kor_aper", typeof(float));             // 언어 반영비율
                dt.Columns.Add("eng_aper", typeof(float));             // 외국어 반영비율  

                dt.Columns.Add("engPerManjum", typeof(bool));
                dt.Columns.Add("engGraAddScore", typeof(bool));
                dt.Columns.Add("korHisGraAddScore", typeof(bool));
            }
   
            return dt;
        }

        /// <summary>
        /// 내신산출 지원등급 조회
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="query"></param>
        /// <param name="step"></param>
        /// <param name="savePath"></param>
        /// <param name="isCollege"></param>
        /// <param name="is3GradeResult"></param>
        public static void MakeApplyScore(string stuId, string query, string step, string savePath, bool isCollege,bool isNesin32)
        {
            // 기존 산출로직 DB 커넥션
            WebConnection webConnection = new WebConnection(true);
            UnivSn_J univSn_J = new UnivSn_J(webConnection);

            //성적조회
            List<NSScore> lstScore = CalcNs.GetNesinScore(stuId, true, isNesin32);

            //마스터테이블조회
            DataTable dt = QueryMasterHak(univSn_J,query);
            string resultContens = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                List<NSScore> nsScores = new List<NSScore>();
                foreach (NSScore nsScore in lstScore)
                {
                    nsScores.Add(nsScore);
                }

                string result = string.Empty;
                result = GetMakeApplyScoreResult(stuId, step, isCollege, dr, nsScores, univSn_J,false);
                resultContens += result;

                Console.WriteLine(result);
            }
            System.IO.File.WriteAllText(savePath, resultContens, Encoding.Default);
        }



        /// <summary>
        /// 학생내신산출 테스트용
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="step"></param>
        /// <param name="savePath"></param>
        /// <param name="isCollege"></param>
        /// <param name="isJungsi"></param>
        public static void VerifyNsCalc(string stuId, string query, string step, string savePath, bool isCollege, bool isJungSi, bool isNesin32)
        {
            // 기존 산출로직 DB 커넥션
            WebConnection webConnection = new WebConnection(true);
            UnivSn_J univSn_J = new UnivSn_J(webConnection);

            //성적조회
            List<NSScore> lstScore = CalcNs.GetNesinScore(stuId, true, isNesin32);
            
            //마스터테이블 조회
            DataTable dt = QueryMasterHak(univSn_J, query);

            //비교내신성적 조회
            NsNonScore nsNonScore = CalcNs.GetFtNsNonFromSql(stuId, "2018");
            string resultContens = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                List<NSScore> nsScores = new List<NSScore>();
                foreach (NSScore nsScore in lstScore)
                {
                    nsScores.Add(nsScore);
                }

                string result = string.Empty;
                result = CalcNsFunc.CheckNsCalc(stuId, step, isCollege, dr, nsScores, nsNonScore, univSn_J, isJungSi);
                resultContens += result;

                Console.WriteLine(result);
            }

            System.IO.File.WriteAllText(savePath, resultContens, Encoding.Default);
        }

        /// <summary>
        /// 내신산출 지원등급 결과 저장
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="step"></param>
        /// <param name="isCollege"></param>
        /// <param name="drUnivCalcInfo"></param>
        /// <param name="nsScoreList"></param>
        /// <param name="univSn_J"></param>
        /// <returns></returns>
        public static string GetMakeApplyScoreResult(string stuId, string step, bool isCollege, DataRow drUnivCalcInfo, List<NSScore> nsScoreList, UnivSn_J univSn_J,bool isJungSi)
        {
            //CS06_SMASTER4 univCalcInfo = new CS06_SMASTER4(drUnivCalcInfo);

            //string info = string.Empty;
            //try
            //{
            //    ApplyScoreResult result = CalcNsFunc.MakeApplyScore(stuId, step, isCollege, drUnivCalcInfo, nsScoreList, univSn_J, isJungSi);
            //    info = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\r\n",
            //                univCalcInfo.idx, univCalcInfo.univ_name,
            //                univCalcInfo.major_name, univCalcInfo.hak_aper1, univCalcInfo.hak_aper2,
            //                result.ApplyGrade,
            //                result.PrevGrade, result.PrevScore,
            //                result.NextGrade, result.NextScore,
            //                result.FinalScore);
            //}
            //catch (Exception ex)
            //{
            //    info = string.Format("{0}\t\t{1}\t{2}\t{3}\t{4}\t{5}\r\n",
            //        univCalcInfo.idx,
            //        univCalcInfo.univ_name,
            //        univCalcInfo.stype_rem,
            //        univCalcInfo.major_name,
            //        "",
            //        ex.Message.ToString()
            //        );
            //}

            //return info;

            return string.Empty;
        }

        /// <summary>
        /// 정시산출 지원가능점수
        /// </summary>
        /// <param name="mstDt"></param>
        /// <returns></returns>
        public static List<UnivScoreResult> GetMakeScoreResult(DataTable mstDt, int stepOrder,bool isCollege23)
        {
                        
            DataTable GradeDt = Proc.CreateCommandText(string.Format("select * from CS06_GRADE_TABLE_JUNGSI_17"));
            List<UnivScoreResult> rstScore = new List<UnivScoreResult>();
            string resultContens = string.Empty;
            foreach (DataRow mstDr in mstDt.Rows)
            {
                try
                {
                    CollegeScholasticAbilityTest csat = new CollegeScholasticAbilityTest(mstDr);

                    UnivScoreResult rst = null;
                    rst = csat.UnivluateCAST(GradeDt, stepOrder,isCollege23);
                    string context = "IDX:" + rst.MasterInfo.idx.ToString() + "\r\n"
                + "대학명:" + rst.MasterInfo.univ_name + "\r\n"
                + "학과명:" + rst.MasterInfo.major_name + "\r\n"
                + "지역코드:" + rst.MasterInfo.univ_area + "\r\n"
                + "전형명:" + rst.MasterInfo.stype_rem + "\r\n"
                + "PARTNAME:" + rst.MasterInfo.part_name + "\r\n"
                + "점수타입:" + rst.MasterInfo.score_type + "\r\n"
                + "기본산출점수 :" + rst.upScore.ToString() + "\r\n"
                + "국어점수 :" + rst.kor.ToString() + "\r\n"
                + "수학점수 :" + rst.math.ToString() + "\r\n"
                + "탐구점수 :" + rst.tam.ToString() + "\r\n"
                + "영어점수 :" + rst.eng.ToString() + "\r\n"
                + "한국사점수 :" + rst.korHis.ToString() + "\r\n"
                + "제2외국어점수 :" + rst.lang2.ToString() + "\r\n"
                + "국어가산점수 :" + rst.AddScores.KorAdd.ToString() + "\r\n"
                + "수학가산점수 :" + rst.AddScores.MathAdd.ToString() + "\r\n"
                + "탐구가산점수 :" + rst.AddScores.TamAdd.ToString() + "\r\n"
                + "영어가산점수 :" + rst.AddScores.EngAdd.ToString() + "\r\n"
                + "한국사가산점수 :" + rst.AddScores.KorHisAdd.ToString() + "\r\n"
                + "제2외국어가산점수 :" + rst.AddScores.Lang2Add.ToString() + "\r\n"
                + "총산출점수:" + rst.MySumScore.ToString();

                    Console.WriteLine(string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), rst.MySumScore));
                    resultContens += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), rst.MySumScore);
                }
                catch(Exception ex)
                {
                    string context = "IDX:" + mstDr["idx"].ToString() + "\r\n"
                                + "대학명:" + mstDr["univ_name"].ToString() + "\r\n"
                                + "소스위치:" + ex.StackTrace.ToString() + "\r\n"
                                + "에러내용:" + ex.Message.ToString();
                    Console.WriteLine(context);
                    resultContens += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString(), mstDr["univ_name"].ToString(), ex.Message);
                }
            
                string fileName = string.Format(@"{0}\{1}_배치점수.txt", Directory, DateTime.Now.ToString("yyyyMMdd"));
                System.IO.File.WriteAllText(fileName, resultContens, Encoding.Default);
            }
            return rstScore;
        }

        /// <summary>
        /// 정시 학생용 테스트용
        /// </summary>
        /// <param name="stuid"></param>
        /// <param name="topid"></param>
        /// <param name="mstDt"></param>
        /// <returns></returns>
        public static DataTable JunSiStuCalc(string stuid, string topid, DataTable mstDt,bool isCollege23,bool isNesin32, string fileName = "")
        {
            DataTable rstDt = new DataTable();

            //수능성적조회
            SnScore sn = CollegeScholasticAbilityTest.SetSnScore(stuid, "20191114_0");
            //내신
            List<NSScore> ns = CalcNs.GetNesinScore(stuid, true, isNesin32);

            //기존산출로직내신객체
            NsScore ftNsScore = GetFtNsScore();
            NsNonScore nsNonScore = CalcNs.GetFtNsNonFromSql(stuid, "2018");
            UserInfo usrInfo = new UserInfo
            {
                StuId = stuid,
                ScoHak = "3",
                StuUseYear = "2018",
                TestDate = "20181115_0",
                TopId = null,
                StuGrade = "2018"
            };
            string resultContens = string.Empty;
            foreach (DataRow mstDr in mstDt.Rows)
            {
                NSCalc master = new NSCalc
                {
                    StypeRem = mstDr["stype_rem"].ToString(),
                    UnivCode = mstDr["univ_code"].ToString(),
                    MajorCode = mstDr["major_code"].ToString(),
                    RtimeCode = mstDr["rtime_code"].ToString(),
                    StypeCode = mstDr["stype_code"].ToString(),
                };

                try
                {
                    DataRow resultDr = CollegeScholasticAbilityTest.CalculateCSAT(usrInfo, master, sn, ftNsScore, nsNonScore, isCollege23);
                    rstDt.ImportRow(resultDr);

                    string context = "IDX:" + mstDr["idx"].ToString()
                        + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                        + "\r\n" + "산출점수:" + resultDr["my_up_score1"].ToString();

                    Console.WriteLine(context);
                    resultContens += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString() 
                        , mstDr["univ_name"].ToString()
                        , resultDr["my_up_score1"].ToString());
                }
                catch (Exception EX)
                {
                    string context = "IDX:" + mstDr["idx"].ToString() + "\r\n"
                             + "\r\n" + "대학명:" + mstDr["univ_name"].ToString()
                             + "\r\n" + "에러내용:" + EX.StackTrace;
                    //Console.WriteLine(context);
                    resultContens += string.Format("{0}\t{1}\t{2}\r\n", mstDr["idx"].ToString() 
                        , mstDr["univ_name"].ToString()
                        , EX.StackTrace);
                }
            }
            MakeRstFile(Directory,fileName, resultContens);

            return rstDt;
        }

        private static string MakeRstFile(string directory,string fileName, string resultContens)
        {
            fileName = string.Format(@"{0}\{1}.txt", directory, fileName);
            System.IO.File.WriteAllText(fileName, resultContens, Encoding.Default);

            return fileName;
        }

        /// <summary>
        /// 내신성적조회
        /// </summary>
        /// <returns></returns>
        private static NsScore GetFtNsScore()
        {
            ArrayList ArrSubScores = new ArrayList();
            string query = "Select * From dbo.CS06_NESIN_CP Where stu_id='DZK00044' and unit is not  null and grade is not null and(sco_hak + part <> '32') order by sco_hak, part";
            DataTable dt = Proc.CreateCommandText(query);
            foreach(DataRow dr in dt.Rows)
            {
                SubjectScore TmpSubScore = new SubjectScore();

                TmpSubScore.WebGbn = Parser.ObjToStr(dr["web_gbn"]);
                TmpSubScore.TopId = Parser.ObjToStr(dr["top_id"]);
                TmpSubScore.StuId = Parser.ObjToStr(dr["stu_id"]);
                TmpSubScore.ScoHak = Parser.ObjToStr(dr["sco_hak"]);
                TmpSubScore.Part = Parser.ObjToStr(dr["part"]);
                TmpSubScore.NssLcode = Parser.ObjToStr(dr["nss_lcode"]);
                TmpSubScore.StuUseYear = Parser.ObjToStr(dr["stu_use_year"]);
                TmpSubScore.Seq = Parser.ObjToStr(dr["seq"]);
                TmpSubScore.CurHak = Parser.ObjToStr(dr["cur_hak"]);
                TmpSubScore.CurBan = Parser.ObjToStr(dr["cur_ban"]);
                TmpSubScore.CurBun = Parser.ObjToStr(dr["cur_bun"]);
                TmpSubScore.CssCode = Parser.ObjToStr(dr["css_code"]);
                TmpSubScore.CssLcode = Parser.ObjToStr(dr["css_lcode"]);
                TmpSubScore.CssName = Parser.ObjToStr(dr["css_name"]);
                TmpSubScore.CssLname = Parser.ObjToStr(dr["css_lname"]);
                TmpSubScore.NssName = Parser.ObjToStr(dr["nss_name"]);
                TmpSubScore.Unit = Parser.ObjToStr(dr["unit"]);
                TmpSubScore.Acc = Parser.ObjToStr(dr["acc"]);
                TmpSubScore.Rank = Parser.ObjToStr(dr["rank"]);
                TmpSubScore.SRank = Parser.ObjToStr(dr["s_rank"]);
                TmpSubScore.Reg = Parser.ObjToStr(dr["reg"]);
                TmpSubScore.OriScore = Parser.ObjToStr(dr["ori_score"]);
                TmpSubScore.Avrg = Parser.ObjToStr(dr["avrg"]);
                TmpSubScore.Std = Parser.ObjToStr(dr["std"]);
                TmpSubScore.Grade = Parser.ObjToStr(dr["grade"]);
                TmpSubScore.CssSubjUnit = Parser.ObjToStr(dr["css_subj_unit"]);

                TmpSubScore.PerM = Parser.GetPerM(dr["rank"], dr["s_rank"], dr["reg"]);

                TmpSubScore.IsModify = false; // DB내용은 아직 수정되지 않았다.

                // 어레이 리스트에 추가한다.
                ArrSubScores.Add(TmpSubScore);
            }

            NsScore ftNsScore = new NsScore(ArrSubScores);

            return ftNsScore;
        }
    }

}
