﻿namespace FutureplanMaster.App.JungSi
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cbUnitCode1 = new System.Windows.Forms.CheckBox();
            this.cbUnitCode2 = new System.Windows.Forms.CheckBox();
            this.cbUnitCode3 = new System.Windows.Forms.CheckBox();
            this.cb_StudentU1 = new System.Windows.Forms.CheckBox();
            this.cb_StudentU2 = new System.Windows.Forms.CheckBox();
            this.txtUnivName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMajorName = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdx = new System.Windows.Forms.TextBox();
            this.chkIsDetail = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtStepOrder = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbCollege23 = new System.Windows.Forms.RadioButton();
            this.rbCollege4 = new System.Windows.Forms.RadioButton();
            this.comboBoxStdYear = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbNesin32 = new System.Windows.Forms.CheckBox();
            this.txtjIdx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "정시학생점수확인",
            "정시지원가능점수"});
            this.comboBox1.Location = new System.Drawing.Point(70, 37);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // cbUnitCode1
            // 
            this.cbUnitCode1.AutoSize = true;
            this.cbUnitCode1.Checked = true;
            this.cbUnitCode1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUnitCode1.Location = new System.Drawing.Point(208, 60);
            this.cbUnitCode1.Name = "cbUnitCode1";
            this.cbUnitCode1.Size = new System.Drawing.Size(64, 16);
            this.cbUnitCode1.TabIndex = 1;
            this.cbUnitCode1.Text = "인문(1)";
            this.cbUnitCode1.UseVisualStyleBackColor = true;
            // 
            // cbUnitCode2
            // 
            this.cbUnitCode2.AutoSize = true;
            this.cbUnitCode2.Checked = true;
            this.cbUnitCode2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUnitCode2.Location = new System.Drawing.Point(208, 82);
            this.cbUnitCode2.Name = "cbUnitCode2";
            this.cbUnitCode2.Size = new System.Drawing.Size(64, 16);
            this.cbUnitCode2.TabIndex = 2;
            this.cbUnitCode2.Text = "자연(2)";
            this.cbUnitCode2.UseVisualStyleBackColor = true;
            // 
            // cbUnitCode3
            // 
            this.cbUnitCode3.AutoSize = true;
            this.cbUnitCode3.Location = new System.Drawing.Point(208, 104);
            this.cbUnitCode3.Name = "cbUnitCode3";
            this.cbUnitCode3.Size = new System.Drawing.Size(76, 16);
            this.cbUnitCode3.TabIndex = 3;
            this.cbUnitCode3.Text = "예채능(3)";
            this.cbUnitCode3.UseVisualStyleBackColor = true;
            // 
            // cb_StudentU1
            // 
            this.cb_StudentU1.AutoSize = true;
            this.cb_StudentU1.Location = new System.Drawing.Point(325, 62);
            this.cb_StudentU1.Name = "cb_StudentU1";
            this.cb_StudentU1.Size = new System.Drawing.Size(72, 16);
            this.cb_StudentU1.TabIndex = 4;
            this.cb_StudentU1.Text = "인문학생";
            this.cb_StudentU1.UseVisualStyleBackColor = true;
            // 
            // cb_StudentU2
            // 
            this.cb_StudentU2.AutoSize = true;
            this.cb_StudentU2.Checked = true;
            this.cb_StudentU2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_StudentU2.Location = new System.Drawing.Point(325, 84);
            this.cb_StudentU2.Name = "cb_StudentU2";
            this.cb_StudentU2.Size = new System.Drawing.Size(72, 16);
            this.cb_StudentU2.TabIndex = 5;
            this.cb_StudentU2.Text = "자연학생";
            this.cb_StudentU2.UseVisualStyleBackColor = true;
            // 
            // txtUnivName
            // 
            this.txtUnivName.Location = new System.Drawing.Point(69, 118);
            this.txtUnivName.Name = "txtUnivName";
            this.txtUnivName.Size = new System.Drawing.Size(121, 21);
            this.txtUnivName.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(386, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "시작";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "대학명 : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "학과명 : ";
            // 
            // txtMajorName
            // 
            this.txtMajorName.Location = new System.Drawing.Point(69, 145);
            this.txtMajorName.Name = "txtMajorName";
            this.txtMajorName.Size = new System.Drawing.Size(121, 21);
            this.txtMajorName.TabIndex = 9;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(15, 242);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(446, 368);
            this.txtResult.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "IDX : ";
            // 
            // txtIdx
            // 
            this.txtIdx.Location = new System.Drawing.Point(69, 63);
            this.txtIdx.Name = "txtIdx";
            this.txtIdx.Size = new System.Drawing.Size(121, 21);
            this.txtIdx.TabIndex = 13;
            // 
            // chkIsDetail
            // 
            this.chkIsDetail.AutoSize = true;
            this.chkIsDetail.Checked = true;
            this.chkIsDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsDetail.Location = new System.Drawing.Point(15, 220);
            this.chkIsDetail.Name = "chkIsDetail";
            this.chkIsDetail.Size = new System.Drawing.Size(100, 16);
            this.chkIsDetail.TabIndex = 14;
            this.chkIsDetail.Text = "자세하게 보기";
            this.chkIsDetail.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "작업유형:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "계열(unit_code)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(318, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "성적표 유형";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "단계:";
            // 
            // txtStepOrder
            // 
            this.txtStepOrder.Location = new System.Drawing.Point(69, 191);
            this.txtStepOrder.Name = "txtStepOrder";
            this.txtStepOrder.Size = new System.Drawing.Size(121, 21);
            this.txtStepOrder.TabIndex = 18;
            this.txtStepOrder.Text = "1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbCollege23);
            this.panel1.Controls.Add(this.rbCollege4);
            this.panel1.Location = new System.Drawing.Point(16, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(141, 30);
            this.panel1.TabIndex = 20;
            // 
            // rbCollege23
            // 
            this.rbCollege23.AutoSize = true;
            this.rbCollege23.Location = new System.Drawing.Point(66, 7);
            this.rbCollege23.Name = "rbCollege23";
            this.rbCollege23.Size = new System.Drawing.Size(59, 16);
            this.rbCollege23.TabIndex = 1;
            this.rbCollege23.Text = "23년제";
            this.rbCollege23.UseVisualStyleBackColor = true;
            // 
            // rbCollege4
            // 
            this.rbCollege4.AutoSize = true;
            this.rbCollege4.Checked = true;
            this.rbCollege4.Location = new System.Drawing.Point(7, 7);
            this.rbCollege4.Name = "rbCollege4";
            this.rbCollege4.Size = new System.Drawing.Size(53, 16);
            this.rbCollege4.TabIndex = 0;
            this.rbCollege4.TabStop = true;
            this.rbCollege4.Text = "4년제";
            this.rbCollege4.UseVisualStyleBackColor = true;
            // 
            // comboBoxStdYear
            // 
            this.comboBoxStdYear.FormattingEnabled = true;
            this.comboBoxStdYear.Items.AddRange(new object[] {
            "2017",
            "2018"});
            this.comboBoxStdYear.Location = new System.Drawing.Point(279, 8);
            this.comboBoxStdYear.Name = "comboBoxStdYear";
            this.comboBoxStdYear.Size = new System.Drawing.Size(70, 20);
            this.comboBoxStdYear.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(169, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "최고점수 적용연도:";
            // 
            // cbNesin32
            // 
            this.cbNesin32.AutoSize = true;
            this.cbNesin32.Location = new System.Drawing.Point(355, 11);
            this.cbNesin32.Name = "cbNesin32";
            this.cbNesin32.Size = new System.Drawing.Size(98, 16);
            this.cbNesin32.TabIndex = 23;
            this.cbNesin32.Text = "내신 3/2 적용";
            this.cbNesin32.UseVisualStyleBackColor = true;
            // 
            // txtjIdx
            // 
            this.txtjIdx.Location = new System.Drawing.Point(69, 91);
            this.txtjIdx.Name = "txtjIdx";
            this.txtjIdx.Size = new System.Drawing.Size(121, 21);
            this.txtjIdx.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "jIDX : ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 615);
            this.Controls.Add(this.txtjIdx);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbNesin32);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxStdYear);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStepOrder);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkIsDetail);
            this.Controls.Add(this.txtIdx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMajorName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtUnivName);
            this.Controls.Add(this.cb_StudentU2);
            this.Controls.Add(this.cb_StudentU1);
            this.Controls.Add(this.cbUnitCode3);
            this.Controls.Add(this.cbUnitCode2);
            this.Controls.Add(this.cbUnitCode1);
            this.Controls.Add(this.comboBox1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox cbUnitCode1;
        private System.Windows.Forms.CheckBox cbUnitCode2;
        private System.Windows.Forms.CheckBox cbUnitCode3;
        private System.Windows.Forms.CheckBox cb_StudentU1;
        private System.Windows.Forms.CheckBox cb_StudentU2;
        private System.Windows.Forms.TextBox txtUnivName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMajorName;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdx;
        private System.Windows.Forms.CheckBox chkIsDetail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtStepOrder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbCollege23;
        private System.Windows.Forms.RadioButton rbCollege4;
        private System.Windows.Forms.ComboBox comboBoxStdYear;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cbNesin32;
        private System.Windows.Forms.TextBox txtjIdx;
        private System.Windows.Forms.Label label9;
    }
}